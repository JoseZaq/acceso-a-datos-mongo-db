package EA2;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.result.UpdateResult;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Updates.combine;
import static com.mongodb.client.model.Updates.set;

public class UpdateProducts {
    static String user = "user";
    static String pass = "M06uf3";
    static String database = "organization";

public static void main(String[] args) {
    MongoClient mongoClient = MongoClients.create("mongodb+srv://" + user + ":" + pass + "@cluster0.knlsx.mongodb.net/retryWrites=true&w=majority");
    MongoDatabase myDB = mongoClient.getDatabase(database);
    MongoCollection collection = myDB.getCollection("people");

    System.out.println("update age quan nom=Mariah Gilmore");
    UpdateResult updateResult = collection.updateOne(eq("name", "Mariah Gilmore"), set("age", 26));
    System.out.println("Registres actualitzats: "+ updateResult.getModifiedCount()+"\n");

    System.out.println("-------------------------------------------");

    System.out.println("update company quan company=Teknoplexon");
    updateResult = collection.updateMany(eq("company", "Teknoplexon"),
                                         set("company", "TeKnoPleXon"));
    System.out.println("Registres actualitzats: "+ updateResult.getModifiedCount()+"\n");

    System.out.println("-------------------------------------------");

    System.out.println("update company i isActive quan company=Techtron");
    //Utilitzem combine per fer dos canvis (1)Company=Tech-Tron i (2)isActive=false
    updateResult = collection.updateMany(eq("company", "Techtron"),
                                                      combine(set("company", "Tech-Tron"),
                                                              set("isActive", false)));
    System.out.println("Registres actualitzats: "+ updateResult.getModifiedCount()+"\n");

}

}
