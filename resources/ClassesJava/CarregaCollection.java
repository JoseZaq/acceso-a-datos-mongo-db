package EA2;

import com.google.gson.Gson;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import EA2.pojo.Person;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;


/**
 * Llegeix un fitxer amb array d'objectes/Documents Ex: [{},{},{}]
 * I fa els insert
 */
public class CarregaCollection {
    static String user = "user";
    static String pass = "M06uf3";
    static String database = "organization";

    public static void main(String[] args) {

        String stringFile = "";
        try (BufferedReader br = new BufferedReader(new FileReader("resources/people.json"))) {
            String linea;
            while ((linea = br.readLine()) != null) {
                stringFile+=linea;
            }
        } catch (FileNotFoundException ex) {
            System.out.println(ex.getMessage());
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
/**/
        Gson gson = new Gson();
        Person[] people = gson.fromJson(stringFile, Person[].class);

        MongoClient mongoClient = MongoClients.create("mongodb+srv://"+user+":"+pass+"@cluster0.knlsx.mongodb.net/retryWrites=true&w=majority");
        MongoDatabase myDB = mongoClient.getDatabase(database);
        MongoCollection<Document> collectionPeople = myDB.getCollection("people");

        for(int i =0; i<people.length; i++) {
            String personJSON = gson.toJson(people[i]);
            System.out.println(personJSON);
            collectionPeople.insertOne(Document.parse(personJSON));
        }

        mongoClient.close();
    }
}
