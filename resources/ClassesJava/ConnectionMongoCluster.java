package practica;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;

public class ConnectionMongoCluster {
    static String user = "invitat";
    static String pass = "sGrhaTY1icJDqrKE";
    static String host = "cluster0.knlsx.mongodb.net";

    private static MongoClient mongoClient;

    public static MongoDatabase getDatabase(String database){
        mongoClient = MongoClients.create("mongodb+srv://"+user+":"+pass+"@"+host+"/retryWrites=true&w=majority");
        MongoDatabase myDB = mongoClient.getDatabase(database);
        return myDB;
    }

    public static void closeConnection(){
        mongoClient.close();
    }
}
