package EA2;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Llegeix un fitxer on cada linea es un objecte/Document {}
 */
public class CarregaDocument {

    static String user = "user";
    static String pass = "M06uf3";
    static String database = "catalog";

    public static void main(String[] args) {
        List<Document> productes = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader("resources/products.json"))) {
            String linea;
            while ((linea = br.readLine()) != null) {
                productes.add(Document.parse(linea));
                System.out.println("producte: "+linea);
            }

        } catch (FileNotFoundException ex) {
            System.out.println(ex.getMessage());
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }

        MongoClient mongoClient = MongoClients.create("mongodb+srv://"+user+":"+pass+"@cluster0.knlsx.mongodb.net/retryWrites=true&w=majority");
        MongoDatabase myDB = mongoClient.getDatabase(database);

        MongoCollection<Document> collectionProducts = myDB.getCollection("products");
        collectionProducts.insertMany(productes);

        mongoClient.close();
    }
}
