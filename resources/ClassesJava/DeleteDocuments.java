package EA2;

import com.mongodb.client.*;
import com.mongodb.client.result.DeleteResult;
import org.bson.Document;

import java.security.cert.CollectionCertStoreParameters;

import static com.mongodb.client.model.Filters.eq;

public class DeleteDocuments {
    static String user = "user";
    static String pass = "M06uf3";
    static String database = "organization";

    public static void main(String[] args) {
        MongoClient mongoClient = MongoClients.create("mongodb+srv://"+user+":"+pass+"@cluster0.knlsx.mongodb.net/retryWrites=true&w=majority");
        MongoDatabase myDB = mongoClient.getDatabase(database);
        MongoCollection collection = myDB.getCollection("people");

        // Delete One
        collection.deleteOne(eq("name","Sophia Oliver"));

        // Delete Many
        DeleteResult deleteResultCompany = collection.deleteMany(eq("company", "Netseco"));
        System.out.println(deleteResultCompany.getDeletedCount());

        DeleteResult deleteResultFriend = collection.deleteMany(eq("friend.name","Anna Freeman"));
        System.out.println(deleteResultFriend.getDeletedCount());

        mongoClient.close();
    }

    static void busqueda(MongoCollection collection){
        Document doc = new Document("company","Netseco");
        FindIterable<Document> find = collection.find(doc).projection(new Document("company",1));
        find.forEach(document -> System.out.println(document.toJson()));
    }
}
