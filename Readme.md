## How to import/export data to Atlas
[commands](https://cloud.mongodb.com/v2/6215090d49ed980e570cff04#clusters/commandLineTools/acceso-a-datos-mongoDB)
- GENERAL: mongoimport --uri mongodb+srv://josezaq:<PASSWORD>@acceso-a-datos-mongodb.m9as2.mongodb.net/<DATABASE> --collection <COLLECTION> --type <FILETYPE> --file <FILENAME>
-  FOR THIS: mongoimport --uri mongodb+srv://josezaq:ITB2020239@acceso-a-datos-mongodb.m9as2.mongodb.net/M6UF3EA1 --collection students --type csv --headerline --file Baixades/students.csv 

## about updating 
[update guide](https://docs.mongodb.com/drivers/java/sync/current/fundamentals/crud/write-operations/embedded-arrays/)

## projections (include, exclude fields) in mongodb with java
[link](https://docs.mongodb.com/drivers/java/sync/current/usage-examples/findOne/)