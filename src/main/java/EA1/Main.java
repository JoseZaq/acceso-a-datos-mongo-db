package EA1;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.result.InsertManyResult;
import org.bson.Document;
import org.bson.json.JsonMode;
import org.bson.json.JsonWriterSettings;
import org.bson.types.ObjectId;
import utils.Menu;
import utils.ConnectionMongoCluster;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

import static com.mongodb.client.model.Filters.*;
import static java.util.Arrays.asList;

public class Main {
    private static MongoDatabase db = ConnectionMongoCluster.getDatabase("M6UF3EA1");
    private static Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
    public static void main(String[] args) {
        String[] items = {"Ex1. Insert 2 students", "Ex2. Query"};
        Menu menu = new Menu(items, scanner ) {
            @Override
            public void goToMethod(int option) {
                switch (option) {
                    case 1:
                        insertTwoStudents();
                        break;
                    case 2:
                        querySomeStudents();
                        break;
                }
            }
        };
        // starts menu
        menu.start();
    }

    /**
     * Exercici 2. Utilitza la classe d'exemple BuscarStudents.java i realitzar les següents cerques:
     * - Mostra les dades dels estudiants del teu grup.
     * - Mostra les dades del estudiants que tenen un 100 al exam
     * - Mostra les dades dels estudiants que tenen menys d’un 50 al exam
     * - Mostra els interessos del estudiants amb student_id=123.
     */
    private static void querySomeStudents() {
        MongoCollection<Document> mongoGrades = db.getCollection("grades");
        Menu submenu = new Menu(new String[]{"From my group","exam = 100", "exam < 50","interests from student 123"}, scanner ){
            @Override
            public void goToMethod(int option) {
                FindIterable<Document> queryGrades;
                var settings = JsonWriterSettings.builder()
                        .indent(true)
                        .outputMode(JsonMode.SHELL)
                        .build();
                switch (option) {
                    case 1:
                        // de mi grupo "Robotica"
                        queryGrades = mongoGrades.find(new Document("group","Robotica"));
                        queryGrades.forEach(document -> System.out.println(document.toJson(settings)));
                        break;
                    case 2:
                        // 100 en el examen
                        queryGrades = mongoGrades.find(eq("scores",Document.parse("{'type':'exam','score':100}")));
                        queryGrades.forEach(document -> System.out.println(document.toJson(settings)));
                        break;
                    case 3:
                        // < 50 al exam
                        queryGrades = mongoGrades.find(lt("scores",Document.parse("{'type':'exam','score':50}")));
                        queryGrades.forEach(document -> System.out.println(document.toJson(settings)));
                        break;
                    case 4:
                        // intereses del estudiante 123
                        queryGrades = mongoGrades.find(new Document("student_id",123));
                        for (Document student : queryGrades) {
                            List<Object> interests = student.getList("interests", Object.class);
                            interests.forEach( document -> System.out.println(interests));
                        }
                        break;
                }
            }
        };
        submenu.start();
    }

    /**
     * Exercici 1. Utilitza la classe d'exemple InsertStudents.
     * java per insertar dos estudiants més a la col·lecció "grades"
     */
    private static void insertTwoStudents() {
        MongoCollection<Document> mongoGrades = db.getCollection("grades");
        Document student1 = new Document("_id",new ObjectId());
        Document student2 = new Document("_id",new ObjectId());

//        student1.append("firstname","Jose")
//                .append("lastname1","Zaquinaula")
//                .append("lastname2","Ramon")
//                .append("dni","46433081Z")
//                .append("gender","H")
//                .append("email","jose.zaquinaula.7e4@itb.cat")
//                .append("phone","722241571")
//                .append("phone_aux","722458695")
//                .append("birth_year",1998);

        student1.append("student_id", 111)
                .append("name", "Jose")
                .append("surname", "Zaquinaula")
                .append("class_id", "DAM")
                .append("group", "Robotica")
                .append("scores",asList(
                        new Document("type", "exam").append("score", 100),
                        new Document("type", "teamwork").append("score", 50)
                ));
        student2.append("student_id", 123)
                .append("name", "Jordi")
                .append("surname", "Naranjo")
                .append("class_id", "undefined")
                .append("group", "Robotica")
                .append("interests",asList("music", "gym", "code", "electronics"));

        List<Document> students = new ArrayList<>();
        students.add(student1);
        students.add(student2);
        InsertManyResult result = mongoGrades.insertMany(students); // insert to ddbb
        result.getInsertedIds().forEach((x,y) -> System.out.println(x+" "+y));
        System.out.println("Estudiantes ingresados con exito!");
    }
}
