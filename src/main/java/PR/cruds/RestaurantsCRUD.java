package PR.cruds;

import PR.connections.MongoClusterConnection;
import PR.model.Restaurant;
import com.google.gson.Gson;
import com.mongodb.DuplicateKeyException;
import com.mongodb.MongoClientSettings;
import com.mongodb.MongoException;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.*;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.InsertManyResult;
import com.mongodb.client.result.InsertOneResult;
import com.mongodb.client.result.UpdateResult;
import org.bson.Document;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.bson.conversions.Bson;
import utils.ConnectionMongoCluster;
import utils.Printer;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

import static com.mongodb.client.model.Accumulators.min;
import static com.mongodb.client.model.Accumulators.sum;
import static com.mongodb.client.model.Aggregates.*;
import static com.mongodb.client.model.Filters.*;
import static com.mongodb.client.model.Updates.set;
import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

public class RestaurantsCRUD {
    public static final String databaseName = "itb";
    public static final String collectionName = "restaurants";
    public static final CodecRegistry restaurantsRegister = fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
            fromProviders(PojoCodecProvider.builder().automatic(true).build()));

    /**
     * delete restaurant collection from Mongo DDBB and show a list of the rest of collections of the database
     * @param databaseName database name where is the collection
     * @param collectionName collection name to drop
     */
    public static void dropCollection(String databaseName, String collectionName) {
        MongoDatabase db = MongoClusterConnection.getDatabase(databaseName);
        MongoCollection<Document> collection = db.getCollection(collectionName);
        // count
        System.out.println("documents of the collection "+collectionName+" from the database "+databaseName+": "+
                collection.countDocuments());
        // delete
        collection.drop();
        System.out.println("collection "+collectionName+" deleted...");
        // show rest of collection
        System.out.println("\nRest of Collections from the database "+databaseName+":");
        db.listCollectionNames().forEach(colName -> System.out.println("\t -"+colName));
    }

    /**
     * Insert restaurants in the restaurants collection from a json file
     * @param databaseName database name where is the collection
     * @param collectionName collection name where add the new restaurants
     * @param file path where is the json file
     */
    public static void insertFromJsonFile(String databaseName, String collectionName, String file) {
        // gson
        Gson gson = new Gson();
        // file
        File fileF = new File(file);
        // readers
        String restaurant = "";
        try {
            Scanner reader  = new Scanner(fileF);
            while(reader.hasNextLine()){
                restaurant += reader.nextLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        // save in object Product
        Restaurant[] restaurants = gson.fromJson(restaurant, Restaurant[].class);
        // insert
        for (Restaurant res : restaurants) {
            System.out.println(res);
        }
        MongoDatabase db = MongoClusterConnection.getDatabase(databaseName).withCodecRegistry(restaurantsRegister);
        MongoCollection<Restaurant> collection = db.getCollection(collectionName, Restaurant.class);
        InsertManyResult result = collection.insertMany(Arrays.asList(restaurants));
        System.out.println("Restaurants succesfully inserted!\nshowing id for each inserted object: ");
        System.out.println("# objects inserted: "+result.getInsertedIds().size()+"\n");

    }

    /**
     * Add a new restaurant in a collection
     * @param database database name
     * @param collection collection name
     * @param restaurant a bson document with the new restaurant
     */
    public static void insert(String database, String collection, Document restaurant) {
        MongoDatabase db = MongoClusterConnection.getDatabase(database);
        MongoCollection<Document> col = db.getCollection(collection);
        try {
            InsertOneResult result = col.insertOne(restaurant);
            System.out.println("Restaurant succesfully inserted!\nshowing id for the object: ");
            System.out.println(" Object id: "+ Objects.requireNonNull(result.getInsertedId()).asObjectId().getValue());
        } catch (MongoException e) {
            System.out.println("\n\u001B[31m"+e.getMessage()+"\033[0m");
        }
    }

    /**
     * Get a restaurant by borough and cuisine
     * @param borough borough string
     * @param cuisine cuisine string
     * @return a restaurant object
     */
    public static List<Document> getRestaurantsByBoroughAndCuisine(String borough, String cuisine) {
        MongoDatabase db = MongoClusterConnection.getDatabase(databaseName);
        MongoCollection<Document> collection = db.getCollection(collectionName);
        List<Document> restaurants = new ArrayList<>();
        // query
        FindIterable<Document> result = collection.find(and(
                eq("borough",borough),
                eq("cuisine",cuisine)
        ));
        result.forEach(restaurants::add);
        return restaurants;
    }

    public static Restaurant getRestaurantsById(String id) {
        MongoDatabase db = MongoClusterConnection.getDatabase(databaseName).withCodecRegistry(restaurantsRegister);
        MongoCollection<Restaurant> collection = db.getCollection(collectionName, Restaurant.class);
        // query
        Restaurant result = collection.find(eq("restaurant_id",id)).first();
        return result;
    }

    /**
     * Get a restaurant by zipcode
     * @param zipcode string of a zipcode
     * @return a restaurant
     */
    public static List<Document> getByZipcode(String zipcode) {
        MongoDatabase db = MongoClusterConnection.getDatabase(databaseName);
        MongoCollection<Document> collection = db.getCollection(collectionName);
        List<Document> restaurants = new ArrayList<>();
        // query
        FindIterable<Document> result = collection.find(eq("address.zipcode",zipcode));
        result.forEach(restaurants::add);
        return restaurants;
    }

    /**
     * Get restaurant names by neighbourhood and excluding all that has an X cuisine
     * @param borough neighbourhood name
     * @param excludeCuisine name of cuisine
     * @return a restaurant list
     */
    public static List<Document> getNameByBoroughNoAmericanCuisine(String borough, String excludeCuisine) {
        MongoDatabase db = MongoClusterConnection.getDatabase(databaseName);
        MongoCollection<Document> collection = db.getCollection(collectionName);
        List<Document> restaurants = new ArrayList<>();
        // query
        Bson projection = Projections.fields(Projections.excludeId(), Projections.include("name"));
        FindIterable<Document> result = collection.find(and(
                eq("borough",borough), ne("cuisine",excludeCuisine)
        )).projection(projection);
        result.forEach(restaurants::add);
        return restaurants;
    }

    /**
     * Get restaurants names by building
     * @param building name of building
     * @return a restaurant list
     */
    public static List<Document> getNameByBuilding(String building) {
        MongoDatabase db = MongoClusterConnection.getDatabase(databaseName);
        MongoCollection<Document> collection = db.getCollection(collectionName);
        List<Document> restaurants = new ArrayList<>();
        // query
        Bson projection = Projections.fields(Projections.excludeId(), Projections.include("name","cuisine"));
        FindIterable<Document> result = collection.find(eq("address.building",building)).projection(projection);
        result.forEach(restaurants::add);
        return restaurants;
    }

    /**
     * Set a number of stars in the stars field to all restaurant with the requeted cuisine
     * @param stars string with the stars to add
     * @param cuisine cuisine name
     * @return number of restaurants updated
     */
    public static long setStars(String stars, String cuisine) {
        MongoDatabase db = MongoClusterConnection.getDatabase("itb").withCodecRegistry(restaurantsRegister);
        MongoCollection<Restaurant> collection = db.getCollection(collectionName, Restaurant.class);
        UpdateResult result = collection.updateMany(eq("cuisine","Caribbean"),set("stars",stars));
        System.out.println("add stars field to all restaurants with "+cuisine+" cuisine");
        return result.getModifiedCount();
    }

    /**
     * Set a new zipcode to all restaurant with the street requested
     * @param street street name
     * @param zipcode new zipcode
     * @return number of restaurants updated
     */
    public static long setZipcode(String street, String zipcode) {
        MongoDatabase db = MongoClusterConnection.getDatabase("itb").withCodecRegistry(restaurantsRegister);
        MongoCollection<Restaurant> collection = db.getCollection(collectionName, Restaurant.class);
        UpdateResult result = collection.updateMany(eq("address.street",street),set("address.zipcode",zipcode));
        System.out.println("update zipcode field to "+ zipcode +" to all restaurants with "+street+" street name");
        return result.getModifiedCount();
    }

    /**
     * Update coordinates to a restaurant by restaurant_id
     * @param restaurantId id
     * @param coordinates new coordinates
     *
     */
    public static void setCoordinatesById(String restaurantId, List<Double> coordinates) {
        MongoDatabase db = MongoClusterConnection.getDatabase("itb").withCodecRegistry(restaurantsRegister);
        MongoCollection<Restaurant> collection = db.getCollection(collectionName, Restaurant.class);
        UpdateResult result = collection.updateMany(eq("restaurant_id",restaurantId),set("address.coordinates",coordinates));
        Printer.printResult("update coordinates field to "+ coordinates +" to all restaurants with "+restaurantId+" restaurantId name");
    }



    /**
     * Create an index of unique key and print all indexes from the collection
     * @param key field to be have the index
     */
    public static void createUniqueKeyIndex(String key) {
        MongoDatabase db = MongoClusterConnection.getDatabase(databaseName).withCodecRegistry(restaurantsRegister);
        MongoCollection<Restaurant> collection = db.getCollection(collectionName, Restaurant.class);
        // indexing
        try{
            IndexOptions indexOptions = new IndexOptions().unique(true);
            String resultCreateIndex =  collection.createIndex(Indexes.ascending(key),indexOptions);
            System.out.printf("Index created: %s%n", resultCreateIndex);
            // print
            collection.listIndexes().forEach(System.out::println);
        } catch (DuplicateKeyException e) {
            System.out.printf("duplicate field values encountered, couldn't create index: \t%s\n", e);
        }
    }

    /**
     * delete restaurants by cuisine
     * @param cuisine cuisine name
     */
    public static void deleteByCuisine(String cuisine) {
        MongoDatabase db = MongoClusterConnection.getDatabase("itb");
        MongoCollection<Document> collection = db.getCollection(collectionName);
        // delete
        try {
            DeleteResult result  = collection.deleteMany(eq("cuisine",cuisine));
            Printer.printResult("restaurants with cuisine " + cuisine +" deleted successfully");
                    Printer.printBetweenLines("#Deleted-restaurants: "+result.getDeletedCount());
        }catch (MongoException e){
            Printer.printError(e.getMessage());
        }
    }

    /**
     * Get number of grades and the sum of all the scores of a restuarant by id
     * @param id restaurant_id
     * @return document with gradesSize and totalScore
     */
    public static Document getGradesLength(String id) {
        MongoDatabase db = ConnectionMongoCluster.getDatabase("itb");
        MongoCollection<Document> collection = db.getCollection(collectionName);
        Document result;
        //
        Bson projection = project(Projections.fields(Projections.excludeId(),Projections.include("grades.score"),
                Projections.computed("gradesSize",
                        Projections.computed("$size","$grades"))));

        Bson projection2 = project(Projections.fields(Projections.exclude("_id"),Projections.include("gradesSize"),
                Projections.computed("totalScore",
                        Projections.computed("$sum","$_id"))));
        result = collection.aggregate(List.of(match(eq("restaurant_id",id)),
                projection,
                group("$grades.score",sum("gradesSize","$gradesSize")),
                projection2
        )).first();

        return  result;
    }

    // aggregations

    /**
     * Get a list of restaurant grouped by borough
     * @return list of Document
     */
    public static List<Document> getRestaurantsAmountByBorough() {
        MongoDatabase db = ConnectionMongoCluster.getDatabase("itb");
        MongoCollection<Document> collection = db.getCollection(collectionName);
        List<Document> result = new ArrayList<>();
        //
        collection.aggregate(
                List.of(group("$borough",sum("restaurants",1)),
                        sort(Sorts.descending("restaurants"))))
                .into(result);

        return  result;
    }

    /**
     * Get the borough with the least restaurants quantity
     * @return a Document
     */
    public static Document getBoroughWithMinRestaurants() {
        MongoDatabase db = ConnectionMongoCluster.getDatabase("itb");
        MongoCollection<Document> collection = db.getCollection(collectionName);
        Document result;
        //
        result = collection.aggregate(
                        List.of(group("$borough",sum("restaurants",1)),
                                sort(Sorts.ascending("restaurants")))).first();

        return  result;
    }

    /**
     * Get a list of Document  with the cuisine name and the quantity of restaurants with this cuisine
     * @return a list of Document
     */
    public static List<Document> groupByCuisine() {
        MongoDatabase db = ConnectionMongoCluster.getDatabase("itb");
        MongoCollection<Document> collection = db.getCollection(collectionName);
        List<Document> result = new ArrayList<>();
        //
        result = collection.aggregate(
                List.of(group("$cuisine",sum("restaurants",1)),
                        sort(Sorts.ascending("restaurants")))).into(result);

        return  result;
    }

    /**
     * returns a list of restaurants that has at least 7 grades. It is showed the name and the grades quantity
     * @return a list of Document
     */
    public static List<Document> getAllWithAtLeast7Grades() {
        MongoDatabase db = ConnectionMongoCluster.getDatabase("itb");
        MongoCollection<Document> collection = db.getCollection(collectionName);
        List<Document> result = new ArrayList<>();
        //
        Bson fieldsIncluded = Projections.include("name");
        Bson projection = project(Projections.fields(
                Projections.excludeId(),
                fieldsIncluded,
                Projections.computed("grades_quantity",
                        Projections.computed("$size","$grades"))));
        result = collection.aggregate(List.of(
                Aggregates.unwind("$grades"),
                group("$name",sum("grades_quantity",1)),
                match(gte("grades_quantity",7))
        )).into(result);
        return result;
    }

    public static List<Document> getCuisineTypesByBorough() {
        MongoDatabase db = ConnectionMongoCluster.getDatabase("itb");
        MongoCollection<Document> collection = db.getCollection(collectionName);
        List<Document> result = new ArrayList<>();
        //
        result = collection.aggregate(List.of(
                group("$borough", Accumulators.addToSet("_CUISINE","$cuisine"))
        )).into(result);

        return  result;
    }

}
