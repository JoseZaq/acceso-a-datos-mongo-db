package PR;
import PR.cruds.RestaurantsCRUD;
import PR.model.Restaurant;
import org.bson.Document;
import org.bson.json.JsonMode;
import org.bson.json.JsonWriterSettings;
import utils.Menu;
import utils.Printer;

import java.util.*;

public class Main {
    private static final Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
    private static final JsonWriterSettings prettyJson = JsonWriterSettings.builder()
            .indent(true)
            .outputMode(JsonMode.SHELL)
            .build();
    public static void main(String[] args) {
        String[] items = {"Practica1: Querys and Setters",
        "Practica 2: Agregations"};
        Menu menu = new Menu(items, scanner) {
            @Override
            public void goToMethod(int option) {
                switch (option) {
                    case 1:
                        QuerysAndSettersSubmenu();
                        break;
                    case 2:
                        AgregationsSubmenu();
                        break;
                }
            }
        };
        // start
        menu.start();
    }

    /**
     * Submenu of the practice 2, agregations
     */
    private static void AgregationsSubmenu() {
        String[] items = {"Ex1.- Crea un mètode que mostri el total de restaurants per cada barri \"borough\"," +
                "\n\t\ti ordena'ls de més quantitat de restaurants a menys quantitat.",
        "Ex2.- Crea un mètode que mostri el barri \"borough\" que té menys Restaurants. Has d’utilitzar l’operador min.",
        "Ex3.- Crea un mètode que mostri el número de restaurants per tipus de cuina i ordena'ls de forma descendent de la" +
                "\n\t\tcuina amb menys restaurants a la cuina amb més restaurants.",
        "Ex4.- Crea un mètode que mostri mostra els restaurants que tinguin com a mínim 7 valoracions \"grades\". Mostra" +
                "\n\t\tnomés el nom del restaurant i el número de valoracions.",
        "Ex5.- Mostra els noms de tipus de cuina per cada barri \"borough\""};
        Menu menu = new Menu(items, scanner) {
            @Override
            public void goToMethod(int option) {
                switch (option) {
                    case 1:
                        restaurantAmountPerBorough();
                        break;
                    case 2:
                        boroughWithMinRestaurants();
                        break;
                    case 3:
                        groupByCuisine();
                        break;
                    case 4:
                        getAllRtrnWithAtLeast7Grades();
                        break;
                    case 5:
                        getCuisineTypesByBorough();
                        break;
                }
            }
        };
        // start
        menu.start();
    }

    private static void getCuisineTypesByBorough() {
        List<Document> docs = RestaurantsCRUD.getCuisineTypesByBorough();
        Printer.printTitle("TYPES OF CUISINE PER BOROUGH");
        docs.forEach(doc -> System.out.println(doc.toJson(prettyJson)));
    }

    private static void getAllRtrnWithAtLeast7Grades() {
        List<Document> docs = RestaurantsCRUD.getAllWithAtLeast7Grades();
        Printer.printTitle("RESTAURANTS WITH AT LEAST 7 GRADES");
        docs.forEach(doc -> System.out.println(doc.toJson(prettyJson)));
    }

    private static void groupByCuisine() {
        List<Document> docs = RestaurantsCRUD.groupByCuisine();
        Printer.printTitle("QUANTITY OF RESTAURANTS BY THEIR CUISINE");
        docs.forEach(doc -> System.out.println(doc.toJson(prettyJson)));
    }

    private static void boroughWithMinRestaurants() {
        Document result = RestaurantsCRUD.getBoroughWithMinRestaurants();
        Printer.printTitle("BOROUGH WITH THE LEAST NUMBER OF RESTAURANTS");
        System.out.println(result.toJson(prettyJson));
    }

    private static void restaurantAmountPerBorough() {
        List<Document> docs = RestaurantsCRUD.getRestaurantsAmountByBorough();
        Printer.printTitle("TOTAL RESTAURANTS PER BOROUGH");
        docs.forEach(doc -> System.out.println(doc.toJson(prettyJson)));
    }

    /**
     * submenu with the first part of the practice
     */
    private static void QuerysAndSettersSubmenu() {
        String[] items = {"Ex1.- Eliminar la col·lecció i mostri la llista de col·leccions existents." +
                "\n\t\t El mètode rep com a paràmetre la base de dades i la col·lecció que s'ha d'eliminar.",
                "Ex2.- Carregar el contingut del fitxer \"restaurants.json\" a una nova col·lecció anomenada" +
                        "\n\t\t \"restaurants\" a la base de dades \"itb\" utilitzant la llibreria \"gson\".",
                "Ex3.- Insertar dos registres dins la col·lecció \"restaurants\". Pots insertar documents o objectes Restaurant.",
                "Ex4.- Crea un mètode per crear un índex de clau única al camp restaurant_id. i que mostri " +
                        "\n\t\t tots els índexs de la col·lecció \"restaurants\".",
                "Ex5.- Mostrar els restaurants on borough = Manhattan i cuisine = Seafood." +
                        "\n\t\t Utilitza una col·lecció de documents.",
                "Ex6.- Crea un mètode que li passis el zipcode com a paràmetre i et mostri el nom dels Restaurants d'aquest codi postal." +
                        "\n\t\tNomés pots mostrar els noms del restaurants.",
                "Ex7.- Crea un mètode que mostri els restaurants que estiguin situats al barri de \"Queens\" però que " +
                        "\n\t\tel tipus de cuina no sigui \"American\" . Mostrar només els noms del restaurants." +
                        "\n\t\tHas de resoldre l’exercici utilitzant una col·lecció de documents.",
                "Ex8.- Crea un mètode que mostri només el nom i el tipus de cuina del restaurant que al camp \"building\"" +
                        "\n\t\ttingui el valor \"3406\".",
                "Ex9.- Crea un mètode que actualitzi el zipcode\" del carrer \"Charles Street\", el nou \"zipcode\" serà \"30033\"." +
                        "\n\t\ti que mostri el numero de registres actualitzats." +
                        "\n\t\tHas de resoldre l’exercici utilitzant una col·lecció de documents.",
                "Ex10.- Crea un mètode que afegeixi el camp \"stars\" = \"*****\" a tots els Restaurants de cuisine Caribbean," +
                        "\n\t\ti que mostri el numero de registres actualitzats, i que mostri també els registres ja actualitzats." +
                        "\n\t\tHas de resoldre l’exercici utilitzant una col·lecció de documents.",
                "Ex11.- Crea un mètode que modifiqui les coordenades del restaurant amb \"restaurant_id\": \"40368271\". Les noves" +
                        "\n\t\tcoordenades són \"-73.996970, 40.72532\". Fes una consulta a la col·lecció restaurants per comprovar que les" +
                        "\n\t\tcoordenades s’han modificat correctament.",
                "Ex12.- Crea un mètode que elimini tots els restaurants on el cuisine = \"Delicatessen\"." +
                        "\n\t\tMostra el numero de restaurants eliminats. Has de resoldre l’exercici utilitzant una col·lecció de documents.",
                "Ex13.- Crea un mètode que mostri el número de valoracions \"grades\" i la suma de totes les valoracions \"score\" del" +
                        "\n\t\tRestaurant amb \"restaurant_id\" = \"40361521\"."};
        Menu menu = new Menu(items, scanner) {
            @Override
            public void goToMethod(int option) {
                switch (option) {
                    case 1:
                        removeRestaurantCollection();
                        break;
                    case 2:
                        insertJsonInRestaurantsCollection();
                        break;
                    case 3:
                        insertRestaurantListToCollection();
                        break;
                    case 4:
                        createUniqueIndexAndShowAllIndex();
                        break;
                    case 5:
                        query();
                        break;
                    case 6:
                        getRtnByZipcode();
                        break;
                    case 7:
                        getRtnNameByBorough();
                        break;
                    case 8:
                        getRtnByBuilding();
                        break;
                    case 9:
                        changeZipByStreetName();
                        break;
                    case 10:
                        setStarsToCaribbeanRtns();
                        break;
                    case 11:
                        updateCoordinatesById();
                        break;
                    case 12:
                        deleteByCuisine();
                        break;
                    case 13:
                        getGradesAndScore();
                        break;
                }
            }
        };
        // start
        menu.start();
    }

    /**
     * show number of grades and the score of a restaurant by id
     */
    private static void getGradesAndScore() {
        String id = "40361521";
        Document doc = RestaurantsCRUD.getGradesLength(id);
        Printer.printTitle("TOTAL GRADES AND SCORE OF "+"RESTAURANT WITH ID \""+id +"\" ");
        System.out.println(doc.toJson(prettyJson));

    }

    /**
     * delete a restaurant by cuisine
     */
    private static void deleteByCuisine() {
        String cuisine = "Delicatessen";
        RestaurantsCRUD.deleteByCuisine(cuisine);
    }

    /**
     * Update coordenates to a restaurant by id
     */
    private static void updateCoordinatesById() {
        String restaurant_id =  "30075445";
        List<Double> coordinates = List.of(-73.996970, 40.72532);
        RestaurantsCRUD.setCoordinatesById(restaurant_id, coordinates);
        Restaurant restaurant = RestaurantsCRUD.getRestaurantsById(restaurant_id);
        System.out.println(restaurant);
    }

    /**
     * Set a new zipcode to all restaurant that are in the street
     */
    private static void changeZipByStreetName() {
        String street = "Charles Street";
        String zipcode = "30033";
        long count = RestaurantsCRUD.setZipcode(street, zipcode);
        Printer.printBetweenLines("#Restaurants updated: "+count);
    }

    /**
     * Set five starts to all restaurants that has a Caribbean cuisine
     */
    private static void setStarsToCaribbeanRtns() {
        String stars = "*****";
        String cuisine = "Caribbean";
        long result  = RestaurantsCRUD.setStars(stars,cuisine);
        Printer.printBetweenLines("#Restaurants updated: "+result);
    }

    /**
     * get a restaurant by building
     */
    private static void getRtnByBuilding() {
        String building = "3406";
        List<Document> restaurants = RestaurantsCRUD.getNameByBuilding(building);
        Printer.printTitle("RESTAURANTS WITH BUILDING \""+building +"\" " +" ("+restaurants.size()+")");
        restaurants.forEach(doc -> System.out.println(doc.toJson(prettyJson)));
    }

    /**
     * get a restaurant name by a neighborhood excluding the ones that has American cuisine
     */
    private static void getRtnNameByBorough() {
        String borough = "Queens";
        String cuisine = "American";
        List<Document> restaurants = RestaurantsCRUD.getNameByBoroughNoAmericanCuisine(borough, cuisine);
        Printer.printTitle("RESTAURANTS WITH NEIGHBOURHOOD \""+borough +"\" " +
                "EXCLUDING CUISINES \""+cuisine+"\" ("+restaurants.size()+")");
        restaurants.forEach(doc -> System.out.println(doc.toJson(prettyJson)));
    }

    /**
     * Get a restaurant by zipcode
     */
    private static void getRtnByZipcode() {
        String zipcode = "10462";
        List<Document> restaurants = RestaurantsCRUD.getByZipcode(zipcode);
        Printer.printTitle("RESTAURANTS WITH ZIPCODE \""+zipcode +"\" ("+restaurants.size()+")");
        restaurants.forEach(doc -> System.out.println(doc.toJson(prettyJson)));
    }

    /**
     * Make a query where -> borough = Manhattan i cuisine = Seafood. In restaurants collection
     */
    private static void query() {
        List<Document> restaurants = RestaurantsCRUD.getRestaurantsByBoroughAndCuisine("Manhattan","Seafood");
        restaurants.forEach(doc -> System.out.println(doc.toJson()));
    }

    /**
     * Create an index of unique key and print all indexes from the collection
     */
    private static void createUniqueIndexAndShowAllIndex() {
        RestaurantsCRUD.createUniqueKeyIndex("restaurant_id");
    }

    /**
     * insert two restaurants in restaurants collection
     */
    private static void insertRestaurantListToCollection() {
        Document res1 = Document.parse("{\n" +
                "\"address\" : {\n" +
                "\"building\" : \"3850\",\n" +
                "\"street\" : \"Richmond Avenue\",\n" +
                "\"zipcode\" : \"10312\"\n" +
                "},\n" +
                "\"borough\" : \"Staten Island\",\n" +
                "\"cuisine\" : \"Pizza\",\n" +
                "\"name\" : \"David Vaquer - El pizzero espagnol\",\n" +
                "\"restaurant_id\" : \"99999999\"\n" +
                "}");

        Document res2 = Document.parse("{\n" +
                "\"address\" : {\n" +
                "\"building\" : \"24\",\n" +
                "\"coord\" : [\n" +
                "-73.9812198,\n" +
                "40.7509706\n" +
                "],\n" +
                "\"street\" : \"East 39 Street\",\n" +
                "\"zipcode\" : \"10016\"\n" +
                "},\n" +
                "\"borough\" : \"Manhattan\",\n" +
                "\"cuisine\" : \"American \",\n" +
                "\"name\" : \"Joan_Gomez's Hamburgers \",\n" +
                "\"restaurant_id\" : \"999999991\"\n" +
                "}");

        Document[] documents = {res1, res2};
        // inserts
        for (Document document : documents) {
            RestaurantsCRUD.insert(RestaurantsCRUD.databaseName, RestaurantsCRUD.collectionName, document);
        }

    }

    /**
     * Insert restaurants in the restaurants collection from a json file
     */
    private static void insertJsonInRestaurantsCollection() {
        RestaurantsCRUD.insertFromJsonFile(RestaurantsCRUD.databaseName,RestaurantsCRUD.collectionName,"resources/jsons/restaurants.json");
    }

    /**
     * Remove the restaurants collections and show a list of the rest of collections of the database
     */
    private static void removeRestaurantCollection() {
        RestaurantsCRUD.dropCollection(RestaurantsCRUD.databaseName, RestaurantsCRUD.collectionName);
    }

}
