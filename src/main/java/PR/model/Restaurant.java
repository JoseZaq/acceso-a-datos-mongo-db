package PR.model;

import java.util.List;
import java.io.Serializable;

public class Restaurant implements Serializable {
	private Address address;
	private String borough;
	private String cuisine;
	private List<GradesItem> grades;
	private String name;
	private String restaurant_id;

	public void setAddress(Address address){
		this.address = address;
	}

	public Address getAddress(){
		return address;
	}

	public void setBorough(String borough){
		this.borough = borough;
	}

	public String getBorough(){
		return borough;
	}

	public void setCuisine(String cuisine){
		this.cuisine = cuisine;
	}

	public String getCuisine(){
		return cuisine;
	}

	public void setGrades(List<GradesItem> grades){
		this.grades = grades;
	}

	public List<GradesItem> getGrades(){
		return grades;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setRestaurant_id(String restaurant_id){
		this.restaurant_id = restaurant_id;
	}

	public String getRestaurant_id(){
		return restaurant_id;
	}

	@Override
	public String toString(){
		return
				"Restaurant" +
						"\n================="+
						"\n\taddress: '" + address + '\'' +
						"\n\tborough: '" + borough + '\'' +
						"\n\tcuisine: '" + cuisine + '\'' +
						"\n\tgrades: '" + grades + '\'' +
						"\n\tname: '" + name + '\'' +
						"\n\trestaurant_id: '" + restaurant_id + '\''+
						"\n=================\n";
	}
}