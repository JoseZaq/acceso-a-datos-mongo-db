package PR.model;

import java.util.List;
import java.io.Serializable;

public class Address implements Serializable {
	private String building;
	private List<Double> coord;
	private String street;
	private String zipcode;

	public void setBuilding(String building){
		this.building = building;
	}

	public String getBuilding(){
		return building;
	}

	public void setCoord(List<Double> coord){
		this.coord = coord;
	}

	public List<Double> getCoord(){
		return coord;
	}

	public void setStreet(String street){
		this.street = street;
	}

	public String getStreet(){
		return street;
	}

	public void setZipcode(String zipcode){
		this.zipcode = zipcode;
	}

	public String getZipcode(){
		return zipcode;
	}

	@Override
 	public String toString(){
		return 
			"Address{" + 
			"building = '" + building + '\'' + 
			",coord = '" + coord + '\'' + 
			",street = '" + street + '\'' + 
			",zipcode = '" + zipcode + '\'' + 
			"}";
		}
}