package utils;

public class Printer {

    /**
     * imprime una cadena de caracteres entre dos barras
     * @param string cadena de caracteres a imprimir
     */
    public static void printBetweenLines(String string){
        StringBuilder line= new StringBuilder();
        int lineSize = string.length()+8;
        line.append("=".repeat(Math.max(0, lineSize)));
        System.out.println(line+"\n"+"    "+string+"\n"+line);
    }

    public static void printTitle(String string) {
        StringBuilder line= new StringBuilder();
        int lineSize = string.length()+8;
        line.append("=".repeat(Math.max(0, lineSize)));
        System.out.println("\n"+Colors.CYAN+string+"\n"+line+Colors.RESET);
    }
    public static void printResult(String string) {
        System.out.println("\n\033[3m"+Colors.GREEN+string+"\n\033[0m"+Colors.RESET);
    }
    public static void printError(String string) {
        System.out.println("\n"+Colors.RED+string+"\n"+Colors.RESET);
    }
}
