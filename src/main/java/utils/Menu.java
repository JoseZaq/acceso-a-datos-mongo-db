package utils;

import java.util.Scanner;

public  class Menu {
    //vars
    String[] items;
    int option;
    Scanner scanner;
    String title;
    //constructor
    public Menu(String[] items,Scanner scanner) {
        this.items = items;
        this.scanner = scanner;
        title = "MENU PRINCIPAL";
        option = -1;
    }
    public Menu(String[] items,Scanner scanner,String title) {
        this(items,scanner);
        this.title =title;
    }
    // actions
    public void start() {
        while (option != 0){
            show();
            option = choose(scanner);
            goToMethod(option);
        }
    }
    public void show(){
        System.out.println("\n\t\t-- "+title+" --");
        for (int i = 0; i < items.length; i++) {
            System.out.println(i+1 + ". " + items[i] );
        }
    }
    public int choose(Scanner scanner){
        System.out.print("Selecciona una opción(0 para salir): ");
        return scanner.nextInt();
    }
    //override methods
    public void goToMethod(int option){
        //todo
    };
    // getters and setters

    public int getOption() {
        return option;
    }
}
