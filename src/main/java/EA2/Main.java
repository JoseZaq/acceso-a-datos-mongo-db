package EA2;

import EA2.pojo.Person;
import com.google.gson.Gson;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Projections;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import org.bson.Document;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.bson.conversions.Bson;
import org.bson.json.JsonMode;
import org.bson.json.JsonWriterSettings;
import utils.Menu;
import utils.ConnectionMongoCluster;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.mongodb.client.model.Filters.*;
import static com.mongodb.client.model.Updates.push;
import static com.mongodb.client.model.Updates.set;
import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

public class Main {
//    private static final MongoDatabase db = ConnectionMongoCluster.getDatabase("M6UF3EA2");
    private static final Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
    public static void main(String[] args) {
        String[] items = {"Ex1. Insert productes.json & students.json","Ex2. insert json array of people",
        "Ex3. Update some products","Ex4. Querys","Ex5. Delete",
        "Ex6. Eliminar una collection","Extra. insert json array of people (by MongoCollection<Person>)"};

        Menu menu = new Menu(items, scanner ) {
            @Override
            public void goToMethod(int option) {
                switch (option) {
                    case 1:
                        carregaDocuments("itb","products2","products.json");
                        carregaDocuments("itb","students2","students.json");
                        break;
                    case 2:
                        loadPeopleByJsonArray();
                        break;
                    case 3:
                        ex3UpdateProducts();
                        break;
                    case 4:
                        ex4Querys();
                        break;
                    case 5:
                        ex5Delete();
                        break;
                    case 6:
                        ex6DeleteCollection();
                        break;
                    case 7:
                        loadPeopleObjectByJsonArray();
                        break;
                }
            }
        };
        // starts menu
        menu.start();
    }

    /**
     * Crea un mètode per eliminar una col·lecció però abans has d'imprimir el número de documents que hi ha. Passa com
     * a paràmetres la BBDD i la col·lecció. Aquest mètode utilitzarà els mètodes de la classe
     * MongoCollection<Document> per fer-ho.
     */
    private static void ex6DeleteCollection() {
        dropCollection("itb","people2");
    }

    /**
     * show all documents of the collection and drops it
     * @param database name of a database
     * @param collection name of a collection
     */
    private static void dropCollection(String database, String collection) {
        MongoCollection<Document> mgCollection  = ConnectionMongoCluster.getDatabase(database).getCollection(collection);
        // count
        System.out.println("documents of the collection "+collection+" from the database "+database+": "+
                mgCollection.countDocuments());
        // delete
        mgCollection.drop();
        System.out.println("collection deleted...");
    }

    /**
     * ELiminar documents
     */
    private static void ex5Delete() {
        MongoDatabase db = ConnectionMongoCluster.getDatabase("itb");
        MongoCollection<Document> collectionStudents = db.getCollection("students2");
        MongoCollection<Document> collectionProducts = db.getCollection("products2");
        String[] items = new String[]{"Crea un mètode per esborrar els productes que valen entre 400 i 600.",
        "Elimina l’estudiant anomenat Daniel Vintro."};
        Menu menu = new Menu(items,scanner,"EX5-DELETE SOME DOCUMENTS"){
            @Override
            public void goToMethod(int option) {
                DeleteResult result;
                Bson projections;
                var settings = JsonWriterSettings.builder()
                        .indent(true)
                        .outputMode(JsonMode.SHELL)
                        .build();
                switch (option) {
                    case 1:
                        result = collectionProducts.deleteOne(and(gt("price",400),lt("price",600)));
                        System.out.println("products deleted: "+result.getDeletedCount());
                        break;
                    case 2:
                        String name = "Daniel Vintro";
                        result = collectionStudents.deleteOne(eq("firstname",name));
                        System.out.println("student(s) deleted: "+result.getDeletedCount());
                        break;
                }
            }
        };
        menu.start();
        ConnectionMongoCluster.closeConnection();
    }

    /**
     * Exercici 4. Cerques
     */
    private static void ex4Querys() {
        MongoDatabase db = ConnectionMongoCluster.getDatabase("itb");
        MongoCollection<Document> collection = db.getCollection("students2");
        MongoCollection<Document> collectionPeople = db.getCollection("people");
        String[] items = new String[]{"Crea un mètode per mostrar el correu electrònic i els telèfons principals i auxiliar de l’estudiant amb\n" +
                "\"dni\":\"07517306D\". Utilitzar el mètode projection.",
        "Crea un mètode per mostrar totes les dades dels estudiants nascuts l’any 1984.",
        "Crea un mètode que mostri quins són els amics de la persona Jessica Adamson."};
        Menu menu = new Menu(items,scanner,"EX4-QUERY SOME DOCUMENTS"){
            @Override
            public void goToMethod(int option) {
                FindIterable<Document> queryGrades;
                Bson projections;
                var settings = JsonWriterSettings.builder()
                        .indent(true)
                        .outputMode(JsonMode.SHELL)
                        .build();
                switch (option) {
                    case 1:
                        String dni = "07517306D";
                        projections  = Projections.fields(Projections.include("email","phone","phone_aux"),
                                Projections.excludeId());
                        queryGrades = collection.find(eq("dni",dni)).projection(projections);
                        queryGrades.forEach(document -> System.out.println(document.toJson(settings)));
                        break;
                    case 2:
                        int birthYear = 1984;
                        queryGrades = collection.find(eq("birth_year",birthYear));
                        queryGrades.forEach(Main::printPrettyJson);
                        break;
                    case 3:
                        String name = "Jessica Adamson";
                        projections = Projections.fields(Projections.include("friends"),Projections.excludeId());
                        queryGrades = collectionPeople.find(eq("name",name)).projection(projections);
                        System.out.println("amigos de la(s) persona(s) con nombre: "+name+": ");
                        queryGrades.forEach(Main::printPrettyJson);
                        break;
                }
            }
        };
        menu.start();
        ConnectionMongoCluster.closeConnection();
    }

    private static void printPrettyJson(Document document) {
        var settings = JsonWriterSettings.builder()
                .indent(true)
                .outputMode(JsonMode.SHELL)
                .build();
        System.out.println(document.toJson(settings));
    }

    /**
     * Exercici 3. Actualitzar documents
     */
    private static void ex3UpdateProducts() {
        MongoDatabase db = ConnectionMongoCluster.getDatabase("itb");
        MongoCollection<Document> collection = db.getCollection("products2");
        Menu menu = new Menu(new String[]{"Crea un mètode per actualitzar els productes que valen entre 600 i 1000. Posarem el Stock a 150",
                "Crea un mètode per afegir un nou camp anomenat discount amb valor 0.20 als productes que el seu Stock siguimés gran de 100.",
                "Crea un mètode per afegir una nova categoria anomenada smartTV al producte anomenat Apple TV."},scanner,"EX2-UPDATE SOME PRODUCTS"){
            @Override
            public void goToMethod(int option) {
                UpdateResult updateResult;
                switch (option) {
                    case 1:
                        updateResult = collection.updateMany(and(lt("price", 1000),gt("price",600)), set("stock", 150));
                        System.out.println("records updated: "+ updateResult.getModifiedCount()+"\n");
                        break;
                    case 2:
                        updateResult = collection.updateMany(gt("stock",100 ), set("discount", 0.20));
                        System.out.println("records updated: "+ updateResult.getModifiedCount()+"\n");
                        break;
                    case 3:
                        updateResult = collection.updateMany(gt("name","Apple TV" ), push("categories", "smartTV"));
                        System.out.println("records updated: "+ updateResult.getModifiedCount()+"\n");
                        break;
                }
            }
        };
        menu.start();
        ConnectionMongoCluster.closeConnection();
    }

    /**
     * mètode per carregar el contingut del fitxer "people.json" a una nova col·lecció anomenada "people2" a la
     * base de dades itb utilitzant la llibreria "Gson". En les Classes de suport del Classroom trobaras els pojos Person i
     * Friend que et servirà per «castejar» el JSON a Array d’objectes Person.
     */
    private static void loadPeopleByJsonArray() {
        String stringFile = "";
        try (BufferedReader br = new BufferedReader(new FileReader("resources/jsons/people.json"))) {
            String linea;
            while ((linea = br.readLine()) != null) {
                stringFile+=linea;
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        Gson gson = new Gson();
        Person[] people = gson.fromJson(stringFile, Person[].class);

        // insert to mongDB
        MongoDatabase db = ConnectionMongoCluster.getDatabase("itb");
        MongoCollection<Document> collectionPeople = db.getCollection("people2");
        for (Person person : people) {
            String personJSON = gson.toJson(person);
            System.out.println(personJSON);
            collectionPeople.insertOne(Document.parse(personJSON));
        }
        ConnectionMongoCluster.closeConnection();
//        MongoDatabase db = ConnectionMongoCluster.getDatabase("itb");
//        MongoCollection<Person> collectionPeople = db.getCollection("people",Person.class);
//        collectionPeople.insertMany(Arrays.asList(people));
//        ConnectionMongoCluster.closeConnection();
    }

    /**
     * mètode per carregar el contingut del fitxer "people.json" a una nova col·lecció anomenada "people2" a la
     * base de dades itb utilitzant la llibreria "Gson". En les Classes de suport del Classroom trobaras els pojos Person i
     * Friend que et servirà per «castejar» el JSON a Array d’objectes Person.
     */
    private static void loadPeopleObjectByJsonArray() {
        String stringFile = "";
        try (BufferedReader br = new BufferedReader(new FileReader("resources/jsons/people.json"))) {
            String linea;
            while ((linea = br.readLine()) != null) {
                stringFile+=linea;
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        Gson gson = new Gson();
        Person[] people = gson.fromJson(stringFile, Person[].class);

        // insert to mongDB
        // create codec registry for POJOs
        CodecRegistry pojoCodecRegistry = fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
                fromProviders(PojoCodecProvider.builder().automatic(true).build()));

        // get handle to "mydb" database
        MongoDatabase database = ConnectionMongoCluster.getDatabase("itb").withCodecRegistry(pojoCodecRegistry);

        // get a handle to the "people" collection
        MongoCollection<Person> collectionPeople = database.getCollection("people2", Person.class);
        for (Person person : people) {
            collectionPeople.insertOne(person);
        }
        ConnectionMongoCluster.closeConnection();
//        MongoDatabase db = ConnectionMongoCluster.getDatabase("itb");
//        MongoCollection<Person> collectionPeople = db.getCollection("people",Person.class);
//        collectionPeople.insertMany(Arrays.asList(people));
//        ConnectionMongoCluster.closeConnection();
    }

    /**
     * es passi com a paràmetres la BBDD, la col·lecció i el fitxer .JSON que carregui el contingut del
     * file a la database/collection indicada
     * @param database name of ddbb
     * @param collection name of collection
     * @param file name of the file with the
     */
    public static void carregaDocuments(String database, String collection, String file) {
        List<Document> productes = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader("resources/jsons/"+file))) {
            String linea;
            while ((linea = br.readLine()) != null) {
                productes.add(Document.parse(linea));
                System.out.println("producte: "+linea);
            }
            // insert to mognoDB
            MongoDatabase db = ConnectionMongoCluster.getDatabase(database);
            MongoCollection<Document> collectionProducts = db.getCollection(collection);
            collectionProducts.insertMany(productes);
            ConnectionMongoCluster.closeConnection(); // close conections
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
