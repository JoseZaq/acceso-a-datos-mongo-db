package EA4.CRUD;

import EA4.pojo.Country;
import com.google.gson.Gson;
import com.mongodb.DuplicateKeyException;
import com.mongodb.MongoClientSettings;
import com.mongodb.MongoException;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.*;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.InsertManyResult;
import com.mongodb.client.result.InsertOneResult;
import com.mongodb.client.result.UpdateResult;
import org.bson.Document;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.bson.conversions.Bson;
import org.bson.json.JsonMode;
import org.bson.json.JsonWriterSettings;
import utils.ConnectionMongoCluster;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.stream.Collectors;

import static com.mongodb.client.model.Filters.*;
import static com.mongodb.client.model.Updates.*;
import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

public class CountryCRUD {
    private static final String databaseName = "itb";
    private static final String collectionName = "countries";
    private static final CodecRegistry countryRegister = fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
            fromProviders(PojoCodecProvider.builder().automatic(true).build()));

    public static void crearIndex(String database, String collection, String key, boolean unique) {
        MongoDatabase db = ConnectionMongoCluster.getDatabase(database);
        MongoCollection<Document> col = db.getCollection(collection);
        col.createIndex(Indexes.ascending(key), new IndexOptions().unique(unique));
        System.out.println("unique index in "+key+" field created");
    }


    /**
     * delete country collection from Mongo DDBB
     */
    public static void dropCountries() {
        MongoDatabase db = ConnectionMongoCluster.getDatabase(databaseName);
        MongoCollection<Document> collection = db.getCollection(collectionName);
        // count
        System.out.println("documents of the collection "+collectionName+" from the database "+databaseName+": "+
                collection.countDocuments());
        // delete
        collection.drop();
        System.out.println("collection "+collectionName+" deleted...");
    }

    public static void carregaCountries(String database, String collection, String file) {
        dropCountries();
        Country[] countries = getCountriesFromFile(file);
        insertObjects(countries);
    }

    /**
     * Give an array of countries from a json file
     * @param file json file path
     * @return Country[]
     */

    public static Country[] getCountriesFromFile(String file) {
        // gson
        Gson gson = new Gson();
        // file
        File fileF = new File(file);
        // readers
        String country = "";
        try {
            Scanner reader  = new Scanner(fileF);
            while(reader.hasNextLine()){
                country += reader.nextLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        // save in object Product
        return gson.fromJson(country, Country[].class);
    }

    /**
     * insert an array of country to the mongo DDBB
     * @param countries Country[]
     */
    public static void insertObjects(Country[] countries) {
        for (Country country : countries) {
            System.out.println(country);
        }
        MongoDatabase db = ConnectionMongoCluster.getDatabase("itb").withCodecRegistry(countryRegister);
        MongoCollection<Country> collection = db.getCollection(collectionName, Country.class);
        InsertManyResult result = collection.insertMany(Arrays.asList(countries));
        System.out.println("Countries succesfully inserted!\nshowing id for each inserted object: ");
        System.out.println("# objects inserted: "+result.getInsertedIds().size()+"\n");
        result.getInsertedIds().forEach((index, value) -> System.out.println(index + " Object id: "+value.asObjectId().getValue()));
    }

    public static void insertaCountry(String database, String collection, Document country) {
        MongoDatabase db = ConnectionMongoCluster.getDatabase(database);
        MongoCollection<Document> col = db.getCollection(collection);
        try {
            InsertOneResult result = col.insertOne(country);
            System.out.println("Country succesfully inserted!\nshowing id for the object: ");
            System.out.println(" Object id: "+ Objects.requireNonNull(result.getInsertedId()).asObjectId().getValue());
        } catch (MongoException e) {
            System.out.println("\n\u001B[31m"+e.getMessage()+"\033[0m");
        }
    }

    public static List<String> getAllLongDescriptions() {
        MongoDatabase db = ConnectionMongoCluster.getDatabase("itb").withCodecRegistry(countryRegister);
        MongoCollection<Country> collection = db.getCollection(collectionName, Country.class);
        List<String> descriptions = new ArrayList<>();
        //
//        Bson projections  = Projections.fields(Projections.include("email","phone","phone_aux"),
//                Projections.excludeId());
        FindIterable<Country> query = collection.find();

//        query.forEach(country -> descriptions.add(country.getLongDescription()));
        return descriptions;
    }

    public static List<Country> llistaIndex(String database, String collection) {
        MongoDatabase db = ConnectionMongoCluster.getDatabase(database).withCodecRegistry(countryRegister);
        MongoCollection<Country> col= db.getCollection(collection, Country.class);
        List<Country> countries = new ArrayList<>();
        // query
        FindIterable<Country> result = col.find();
        result.forEach(countries::add);
        return countries;
    }

//    public static List<Country> getCountriesByAuthor(String author) {
//        List<Country> countries = getAllCountries();
//        // query
//        List<Country> arrayCountries = countries.stream().filter(country -> country.getAuthors().stream().anyMatch(auth -> auth.equals(author))).collect(Collectors.toList());
//        return arrayCountries;
//    }

    public static List<Country> getCountriesByAuthors(String[] authors) {
        MongoDatabase db = ConnectionMongoCluster.getDatabase("itb").withCodecRegistry(countryRegister);
        MongoCollection<Country> collection = db.getCollection(collectionName, Country.class);
        List<Country> countries = new ArrayList<>();
        // query
        FindIterable<Country> result = collection.find(all("authors",authors));
        result.forEach(countries::add);
        return countries;
    }

    public static List<Country> getCountriesByPagesAndCategory(double max, double min, String category) {
        MongoDatabase db = ConnectionMongoCluster.getDatabase("itb").withCodecRegistry(countryRegister);
        MongoCollection<Country> collection = db.getCollection(collectionName, Country.class);
        List<Country> countries = new ArrayList<>();
        // query
        FindIterable<Country> result = collection.find(and(gte("pageCount",min),lte("pageCount",max)));
        result.forEach(countries::add);
        return countries;
    }

    public static List<Country> getCountriesByCategoryAndDiscardedAuthor(String category, String author) {
        MongoDatabase db = ConnectionMongoCluster.getDatabase("itb").withCodecRegistry(countryRegister);
        MongoCollection<Country> collection = db.getCollection(collectionName, Country.class);
        List<Country> countries = new ArrayList<>();
        // query
        FindIterable<Country> result = collection.find(and(
                        all("categories",category), not(all("author",author))))
                .sort(Sorts.ascending("title"));
        result.forEach(countries::add);
        return countries;
    }

    public static List<Country> getCountriesByCategories(String[] categories, int limit) {
        MongoDatabase db = ConnectionMongoCluster.getDatabase("itb").withCodecRegistry(countryRegister);
        MongoCollection<Country> collection = db.getCollection(collectionName, Country.class);
        List<Country> countries = new ArrayList<>();
        // query
        FindIterable<Country> result = collection.find(in("categories",categories))
                .sort(Sorts.ascending("title"))
                .limit(limit);
        result.forEach(countries::add);
        return countries;
    }
    public static List<String> getAllCategories() {
        MongoDatabase db = ConnectionMongoCluster.getDatabase("itb").withCodecRegistry(countryRegister);
        MongoCollection<Country> collection = db.getCollection(collectionName, Country.class);
        // query
        List<String> categories = new ArrayList<>();
        collection.distinct("categories",String.class).forEach(categories::add);
        return categories;
    }

    public static List<String> getAllAuthors() {
        MongoDatabase db = ConnectionMongoCluster.getDatabase("itb").withCodecRegistry(countryRegister);
        MongoCollection<Country> collection = db.getCollection(collectionName, Country.class);
        // query
        List<String> authors = new ArrayList<>();
        collection.distinct("authors",String.class).forEach(authors::add);
        return authors;
    }

    public static long setAssessment(int value) {
        MongoDatabase db = ConnectionMongoCluster.getDatabase("itb").withCodecRegistry(countryRegister);
        MongoCollection<Country> collection = db.getCollection(collectionName, Country.class);
        UpdateResult result = collection.updateMany(and(in("categories","Internet")
                ,eq("status","PUBLISH")),set("Assessment",value));
        System.out.println("add Assessment field to all publish countries of category Internet ");
        return result.getModifiedCount();
    }

    public static long addAuthor(String author, String title) {
        MongoDatabase db = ConnectionMongoCluster.getDatabase("itb").withCodecRegistry(countryRegister);
        MongoCollection<Country> collection = db.getCollection(collectionName, Country.class);
        Bson query =eq("title",title);
        Bson updates =push("authors",author);
        UpdateResult result = collection.updateMany(query,updates);
        System.out.println("Add "+author+" author in country with title "+title);
        return result.getModifiedCount();
    }

    public static void createUniqueKeyIndex(String key) {
        MongoDatabase db = ConnectionMongoCluster.getDatabase("itb").withCodecRegistry(countryRegister);
        MongoCollection<Country> collection = db.getCollection(collectionName, Country.class);
        // indexing
        try{
            IndexOptions indexOptions = new IndexOptions().unique(true);
            String resultCreateIndex =  collection.createIndex(Indexes.ascending(key),indexOptions);
            System.out.printf("Index created: %s%n", resultCreateIndex);
        } catch (DuplicateKeyException e) {
            System.out.printf("duplicate field values encountered, couldn't create index: \t%s\n", e);
        }
    }

    public static void showAllIndex() {
        MongoDatabase db = ConnectionMongoCluster.getDatabase("itb").withCodecRegistry(countryRegister);
        MongoCollection<Country> collection = db.getCollection(collectionName, Country.class);
        // index
        collection.listIndexes().forEach(System.out::println);
    }

    public static void deleteAllByAuthor(String author) {
        MongoDatabase db = ConnectionMongoCluster.getDatabase("itb").withCodecRegistry(countryRegister);
        MongoCollection<Country> collection = db.getCollection(collectionName, Country.class);
        // delete
        try {
            DeleteResult result  = collection.deleteMany(in("authors",author));
            System.out.println(author+" Countries deleted successfully\n"+
                    "#Deleted-countries: "+result.getDeletedCount());
        }catch (MongoException e){
            System.out.println(e.getMessage());
        }
    }

    public static void deleteByTitle(String title) {
        MongoDatabase db = ConnectionMongoCluster.getDatabase("itb").withCodecRegistry(countryRegister);
        MongoCollection<Country> collection = db.getCollection(collectionName, Country.class);
        // delete
        try {
            DeleteResult result  = collection.deleteOne(eq("title",title));
            System.out.println(title+" country deleted successfully\n"+
                    "#Deleted-countries: "+result.getDeletedCount());
        }catch (MongoException e){
            System.out.println(e.getMessage());
        }
    }

    public static void deleteField(String fieldName) {
        MongoDatabase db = ConnectionMongoCluster.getDatabase("itb").withCodecRegistry(countryRegister);
        MongoCollection<Country> collection = db.getCollection(collectionName, Country.class);
        // delete
        try {
            UpdateResult result = collection.updateMany(ne("_id",-1),unset(fieldName));
            System.out.println(fieldName+" remove from all documents\n#Documents-updated: "+result.getModifiedCount());
        }catch (MongoException e){
            System.out.println(e.getMessage());
        }
    }

    public static void countCountriesByAuthor() {
        MongoDatabase db = ConnectionMongoCluster.getDatabase("itb");
        MongoCollection<Document> collection = db.getCollection(collectionName);
        AggregateIterable<Document> queryGrades;
        var settings = JsonWriterSettings.builder()
                .indent(true)
                .outputMode(JsonMode.SHELL)
                .build();
        List<Bson> pipeline = List.of(Aggregates.unwind("$authors"),Aggregates.group("$authors",Accumulators.sum("countries",1)));
        List<Document> result = new ArrayList<>();
        queryGrades = collection.aggregate(pipeline);
        queryGrades.forEach(document -> System.out.println(document.toJson()));
    }
}
