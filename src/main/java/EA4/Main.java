package EA4;

import EA3.CRUD.BooksCRUD;
import EA3.pojo.Book;
import EA4.CRUD.CountryCRUD;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.*;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.json.JsonMode;
import org.bson.json.JsonWriterSettings;
import utils.ConnectionMongoCluster;
import utils.Menu;

import java.util.*;

import static com.mongodb.client.model.Accumulators.avg;
import static com.mongodb.client.model.Accumulators.sum;
import static com.mongodb.client.model.Aggregates.*;
import static com.mongodb.client.model.Filters.*;

public class Main {
    private static final Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
    private static final JsonWriterSettings settings = JsonWriterSettings.builder()
            .indent(true)
            .outputMode(JsonMode.SHELL)
            .build();
    public static void main(String[] args) {
        String[] items = {"Ex1- Importació dels documents del fitxer countries.json",
                "Ex2- Creació d’Index de clau única","Ex3-Agregacions"};
        Menu menu = new Menu(items, scanner) {
            @Override
            public void goToMethod(int option) {
                switch (option) {
                    case 1:
                        ex1InsertCountries();
                        break;
                    case 2:
                        ex2CreateWithUniqueKey();
                        break;
                    case 3:
                        ex3Arguments();
                        break;
                }
            }
        };
        // start
        menu.start();
    }

    private static void ex3Arguments() {
        String[] items = {"Test the 4 examples","Quants països conté cada subregió «subregion»",
                "Quin és el país on es parlen més idiomes. Ordena els països per número d’idiomes que es parla de més a menys i mostra només el primer.",
                "De la col·lecció «books», mostra tots els llibres que tinguin com a mínim 5 authors.",
                "De la col·lecció «books». Ordena els llibres per número de categories a que pertany de forma descendent.\n" +
                        "Primer els llibres que pertanyen a més categories i al final els llibres que pertanyen a menys categories.\n" +
                        "Limita la sortida a un llibre mostrant el llibre que pertany a més categories.",
                "De la col·lecció «books». Mostra els llibres ordenats per número d’autors de forma descendent. Primer els\n" +
                        "llibres amb més autors i al final els llibres amb menys autors.",
                "De la col·lecció «books». Mostra els ISBN per cada Status \"status\". Utilitza l’estructura aggregate, i\n" +
                        "utilitza les funcions $group i $addToSet.",
                "De la col·lecció «people» Mostra una llista amb el nom de tots els amics de les persones.",
                "De la col·lecció «people». Mostra totes les persones que tinguin 7 o més etiquetes (tags). Utilitza\n" +
                        "l’estructura aggregate. Mostra només el nom de la persona i número d’etiquetes.",
        "De la col·lecció «people». Calcula la mitjana d’edat del homes i la mitjana d’edat de les dones. Utilitza\n" +
                "l’estructura aggregate, i utilitza les funcions $group i $avg. Mostra el gènere (camp \"gender\") i la mitjana\n" +
                "d’edat del gènere (camp \"age\")."};
        Menu menu = new Menu(items, scanner) {
            @Override
            public void goToMethod(int option) {
                switch (option) {
                    case 1:
                        testExamples();
                        break;
                    case 2:
                        maxCountriesOnSubregions();
                        break;
                    case 3:
                        countryWithMaxLanguages();
                        break;
                    case 4:
                        booksWithAtLeast5Authors();
                        break;
                    case 5:
                        bookWithMoreCategories();
                        break;
                    case 6:
                        booksByAuthorsQuantity();
                        break;
                    case 7:
                        ibsnPerStatus();
                        break;
                    case 8:
                        allFriends();
                        break;
                    case 9:
                        peopleWithAtLeast7Tags();
                        break;
                    case 10:
                        peopleAvgAgeByGender();
                        break;
                }
            }
        };
        // start
        menu.start();
    }

    private static void ibsnPerStatus() {
        MongoDatabase db = ConnectionMongoCluster.getDatabase("itb");
        MongoCollection<Document> collection = db.getCollection("books");
        AggregateIterable<Document> result;
        //
        result = collection.aggregate(List.of(
                group("$status",Accumulators.addToSet("_ISBN","$isbn"))
        ));

        System.out.println("ISBN'S PER STATUS\n=======================");
        result.forEach(doc -> System.out.println(doc.toJson(settings)));
    }

    private static void peopleAvgAgeByGender() {
        MongoDatabase db = ConnectionMongoCluster.getDatabase("itb");
        MongoCollection<Document> collection = db.getCollection("people");
        AggregateIterable<Document> femaleResult;
        AggregateIterable<Document> maleResult;
        //
        femaleResult = collection.aggregate(List.of(
                match(eq("gender","female")),
                group("Female",Accumulators.avg("age_avg","$age"))
                ));

        maleResult = collection.aggregate(List.of(
                match(eq("gender","male")),
                group("Male",Accumulators.avg("age_avg","$age"))
                ));

        System.out.println("PEOPLE WITH AT LEAST 7 TAGS\n=======================");
        femaleResult.forEach(doc -> System.out.println(doc.toJson(settings)));
        maleResult.forEach(doc -> System.out.println(doc.toJson(settings)));
    }

    private static void peopleWithAtLeast7Tags() {
        MongoDatabase db = ConnectionMongoCluster.getDatabase("itb");
        MongoCollection<Document> collection = db.getCollection("people");
        AggregateIterable<Document> result;
        //
        Bson fieldsIncluded = Projections.include("name");
        Bson projection = project(Projections.fields(
                Projections.excludeId(),
                fieldsIncluded,
                Projections.computed("tagsSize",
                        Projections.computed("$size","$tags"))));
        result = collection.aggregate(List.of(
                projection,
                match(gte("tagsSize",7))
        ));

        System.out.println("PEOPLE WITH AT LEAST 7 TAGS\n=======================");
        result.forEach(doc -> System.out.println(doc.toJson(settings)));
    }

    private static void allFriends() {
        MongoDatabase db = ConnectionMongoCluster.getDatabase("itb");
        MongoCollection<Document> collection = db.getCollection("people");
        AggregateIterable<Document> result;
        result = collection.aggregate(List.of(group("$friends.name")));
        System.out.println("ALl FRIENDS TO EACH PERSON\n=======================");
        result.forEach(doc -> System.out.println(doc.toJson(settings)));
    }

    private static void booksByAuthorsQuantity() {
        MongoDatabase db = ConnectionMongoCluster.getDatabase("itb");
        MongoCollection<Document> collection = db.getCollection("books");
        AggregateIterable<Document> result;
        Bson booksWith5authors = null;
        //
        Bson fieldsIncluded = Projections.include(
                "title",
                "isbn",
                "pageCount",
                "publishedDate",
                "thumbnailUrl",
                "shortDescription",
                "status",
                "authors",
                "categories");
        Bson projection = project(Projections.fields(
                Projections.computed("authorsSize",
                        Projections.computed("$size","$authors")),
                fieldsIncluded));
        result = collection.aggregate(List.of(projection,sort(Sorts.descending("authorsSize"))));

        System.out.println("BOOKS By AUTHORS QUANTITY\n=======================");
        result.forEach(doc -> System.out.println(doc.toJson(settings)));
    }

    private static void bookWithMoreCategories() {
        MongoDatabase db = ConnectionMongoCluster.getDatabase("itb");
        MongoCollection<Document> collection = db.getCollection("books");
        String title;
        // querying
        Bson projection = project(Projections.fields(Projections.excludeId(),Projections.include("title"),
                Projections.computed("categoriesSize",
                        Projections.computed("$size","$categories"))));
        title = collection.aggregate(List.of(projection, sort(Sorts.descending("categoriesSize")))).first().getString("title");
        // get book and print
        Book book = BooksCRUD.getBooksByTitle(title);
        System.out.println("COUNTRY WITH MOST QUANTITY OF LANGUAGES");
        System.out.println(book);
    }

    private static void booksWithAtLeast5Authors() {
        MongoDatabase db = ConnectionMongoCluster.getDatabase("itb");
        MongoCollection<Document> collection = db.getCollection("books");
        AggregateIterable<Document> result;
        Bson booksWith5authors = null;
        //
        Bson fieldsIncluded = Projections.include("_id",
                "title",
                "isbn",
                "pageCount",
                "publishedDate",
                "thumbnailUrl",
                "shortDescription",
                "status",
                "authors",
                "categories");
        Bson projection = project(Projections.fields(
                Projections.excludeId(),
                fieldsIncluded,
                Projections.computed("authorsSize",
                        Projections.computed("$size","$authors"))));
        Bson projection2 = project(Projections.fields(Projections.exclude("authorsSize")));
        result = collection.aggregate(List.of(
                projection,
                match(gte("authorsSize",5)),
                projection2
        ));

        System.out.println("BOOKS WITH AT LEAST 5 AUTHORS\n=======================");
        result.forEach(doc -> System.out.println(doc.toJson(settings)));
    }


    private static void countryWithMaxLanguages() {
        MongoDatabase db = ConnectionMongoCluster.getDatabase("itb");
        MongoCollection<Document> collection = db.getCollection("countries");
        Document result;
        //
        Bson projection = project(Projections.fields(Projections.excludeId(),Projections.include("name"),
                Projections.computed("languagesSize",
                        Projections.computed("$size","$languages"))));
        result = collection.aggregate(List.of(projection, sort(Sorts.descending("languagesSize")))).first();
        System.out.println("COUNTRY WITH MOST QUANTITY OF LANGUAGES\n=======================");
        System.out.println(Objects.requireNonNull(result).toJson(settings));
    }

    private static void maxCountriesOnSubregions() {
        MongoDatabase db = ConnectionMongoCluster.getDatabase("itb");
        MongoCollection<Document> collection = db.getCollection("countries");
        AggregateIterable<Document> result;
        //
        result = collection.aggregate(List.of(group("$subregion",sum("countries",1))));
        result.forEach(System.out::println);
    }

    private static void testExamples() {
        MongoDatabase db = ConnectionMongoCluster.getDatabase("itb");
        MongoCollection<Document> collection = db.getCollection("countries");
        //
        Document englishSpeakingCountries = collection.aggregate(Arrays.asList(
                match(eq("languages.name", "English")),
                count())).first();
        System.out.println("COUNTRIES WITH ENGLISH AS NATIVE LANGUAGE\n=======================\n"+ Objects.requireNonNull(englishSpeakingCountries));
        //
        Document maxCountriedRegion = collection.aggregate(Arrays.asList(
                group("$region", sum("tally", 1)),
                sort(Sorts.descending("tally")))).first();
        System.out.println("\nREGION WITH THE MAXIMUN NUMBER OF COUNTRIES \n=======================\n"+ Objects.requireNonNull(maxCountriedRegion));
        //
        collection.aggregate(Arrays.asList(
                sort(Sorts.descending("area")),
                limit(7),
                out("largest_seven"))).toCollection();
        MongoCollection<Document> largestSeven = db.getCollection("largest_seven");
        FindIterable<Document> seven = largestSeven.find();
        System.out.println("\nTHE MOST LARGEST COUNTRIES IN THE WORLD \n=======================");
        seven.forEach(System.out::println);
        //
        System.out.println("\nHOW MANY BORDERS EACH COUNTRY SHARE AND WHAT IS THE MAXIMUN BORDERS SHARED \n=======================");
        Bson borderingCountriesCollection = project(Projections.fields(Projections.excludeId(),
                Projections.include("name"), Projections.computed("borderingCountries",
                        Projections.computed("$size", "$borders"))));

        int maxValue = collection.aggregate(Arrays.asList(borderingCountriesCollection,
                        group(null, Accumulators.max("max", "$borderingCountries"))))
                .first().getInteger("max");

        System.out.println("Max value of borders shared: "+maxValue);

        Document maxNeighboredCountry = collection.aggregate(Arrays.asList(borderingCountriesCollection,
                match(eq("borderingCountries", maxValue)))).first();

        System.out.println("Country with max borders shared: "+maxNeighboredCountry);
    }

    private static void ex2CreateWithUniqueKey() {
        CountryCRUD.crearIndex("itb","countries","name",true);
        CountryCRUD.llistaIndex("itb","countries");
        String json = "{\"name\": \"Spain\"}";
        Document document = new Document();
        document = Document.parse(json);
        CountryCRUD.insertaCountry("itb","countries",document);
    }

    private static void ex1InsertCountries() {
        CountryCRUD.carregaCountries("itb","countries","resources/jsons/countries.json");
    }
}
