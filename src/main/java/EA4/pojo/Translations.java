package EA4.pojo;

import java.io.Serializable;

public class Translations implements Serializable {
	private String de;
	private String es;
	private String fr;
	private String ja;
	private String it;
	private String br;
	private String pt;
	private String nl;
	private String hr;
	private String fa;

	public void setDe(String de){
		this.de = de;
	}

	public String getDe(){
		return de;
	}

	public void setEs(String es){
		this.es = es;
	}

	public String getEs(){
		return es;
	}

	public void setFr(String fr){
		this.fr = fr;
	}

	public String getFr(){
		return fr;
	}

	public void setJa(String ja){
		this.ja = ja;
	}

	public String getJa(){
		return ja;
	}

	public void setIt(String it){
		this.it = it;
	}

	public String getIt(){
		return it;
	}

	public void setBr(String br){
		this.br = br;
	}

	public String getBr(){
		return br;
	}

	public void setPt(String pt){
		this.pt = pt;
	}

	public String getPt(){
		return pt;
	}

	public void setNl(String nl){
		this.nl = nl;
	}

	public String getNl(){
		return nl;
	}

	public void setHr(String hr){
		this.hr = hr;
	}

	public String getHr(){
		return hr;
	}

	public void setFa(String fa){
		this.fa = fa;
	}

	public String getFa(){
		return fa;
	}

	@Override
 	public String toString(){
		return 
			"Translations{" + 
			"de = '" + de + '\'' + 
			",es = '" + es + '\'' + 
			",fr = '" + fr + '\'' + 
			",ja = '" + ja + '\'' + 
			",it = '" + it + '\'' + 
			",br = '" + br + '\'' + 
			",pt = '" + pt + '\'' + 
			",nl = '" + nl + '\'' + 
			",hr = '" + hr + '\'' + 
			",fa = '" + fa + '\'' + 
			"}";
		}
}