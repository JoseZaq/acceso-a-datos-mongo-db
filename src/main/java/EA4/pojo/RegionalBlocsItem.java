package EA4.pojo;

import java.util.List;
import java.io.Serializable;

public class RegionalBlocsItem implements Serializable {
	private String acronym;
	private String name;
	private List<String> otherAcronyms;
	private List<String> otherNames;

	public void setAcronym(String acronym){
		this.acronym = acronym;
	}

	public String getAcronym(){
		return acronym;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setOtherAcronyms(List<String> otherAcronyms){
		this.otherAcronyms = otherAcronyms;
	}

	public List<String> getOtherAcronyms(){
		return otherAcronyms;
	}

	public void setOtherNames(List<String> otherNames){
		this.otherNames = otherNames;
	}

	public List<String> getOtherNames(){
		return otherNames;
	}

	@Override
 	public String toString(){
		return 
			"RegionalBlocsItem{" + 
			"acronym = '" + acronym + '\'' + 
			",name = '" + name + '\'' + 
			",otherAcronyms = '" + otherAcronyms + '\'' + 
			",otherNames = '" + otherNames + '\'' + 
			"}";
		}
}