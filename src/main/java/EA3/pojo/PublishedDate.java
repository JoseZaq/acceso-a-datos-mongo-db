package EA3.pojo;

import java.io.Serializable;

public class PublishedDate implements Serializable {
	private String date;

	public void setDate(String date){
		this.date = date;
	}

	public String getDate(){
		return date;
	}

	@Override
 	public String toString(){
		return 
			"PublishedDate{" + 
			"$date = '" + date + '\'' + 
			"}";
		}
}