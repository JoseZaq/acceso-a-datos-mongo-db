package EA3.pojo;

import java.util.List;
import java.io.Serializable;

public class Book implements Serializable {
	private int _id;
	private String title;
	private String isbn;
	private int pageCount;
	private PublishedDate publishedDate;
	private String thumbnailUrl;
	private String shortDescription;
	private String longDescription;
	private String status;
	private List<String> authors;
	private List<String> categories;

	public void set_id(int _id){
		this._id = _id;
	}

	public int get_id(){
		return _id;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return title;
	}

	public void setIsbn(String isbn){
		this.isbn = isbn;
	}

	public String getIsbn(){
		return isbn;
	}

	public void setPageCount(int pageCount){
		this.pageCount = pageCount;
	}

	public int getPageCount(){
		return pageCount;
	}

	public void setPublishedDate(PublishedDate publishedDate){
		this.publishedDate = publishedDate;
	}

	public PublishedDate getPublishedDate(){
		return publishedDate;
	}

	public void setThumbnailUrl(String thumbnailUrl){
		this.thumbnailUrl = thumbnailUrl;
	}

	public String getThumbnailUrl(){
		return thumbnailUrl;
	}

	public void setShortDescription(String shortDescription){
		this.shortDescription = shortDescription;
	}

	public String getShortDescription(){
		return shortDescription;
	}

	public void setLongDescription(String longDescription){
		this.longDescription = longDescription;
	}

	public String getLongDescription(){
		return longDescription;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	public void setAuthors(List<String> authors){
		this.authors = authors;
	}

	public List<String> getAuthors(){
		return authors;
	}

	public void setCategories(List<String> categories){
		this.categories = categories;
	}

	public List<String> getCategories(){
		return categories;
	}

	@Override
 	public String toString(){
		return
			"\n===============\n"+
			"_id: " + _id + '\n' +
			"title: " + title + '\n' +
			"isbn: " + isbn + '\n' +
			"pageCount: " + pageCount + '\n' +
			"publishedDate: " + publishedDate + '\n' +
			"thumbnailUrl: " + thumbnailUrl + '\n' +
			"shortDescription: " + shortDescription + '\n' +
			"longDescription: " + longDescription + '\n' +
			"status: " + status + '\n' +
			"authors: " + authors + '\n' +
			"categories: " + categories + '\n';
		}
}