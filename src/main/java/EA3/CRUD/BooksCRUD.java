package EA3.CRUD;

import EA3.pojo.Book;
import com.google.gson.Gson;
import com.mongodb.DuplicateKeyException;
import com.mongodb.MongoClientSettings;
import com.mongodb.MongoException;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.*;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.InsertManyResult;
import com.mongodb.client.result.InsertOneResult;
import com.mongodb.client.result.UpdateResult;
import org.bson.Document;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.bson.conversions.Bson;
import org.bson.json.JsonMode;
import org.bson.json.JsonWriterSettings;
import utils.ConnectionMongoCluster;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.stream.Collectors;

import static com.mongodb.client.model.Filters.*;
import static com.mongodb.client.model.Updates.*;
import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

public class BooksCRUD {
    private static final String databaseName = "itb";
    private static final String collectionName = "books";
    private static final CodecRegistry bookRegistry = fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
            fromProviders(PojoCodecProvider.builder().automatic(true).build()));

    /**
     * delete book collection from Mongo DDBB
     */
    public static void dropBooks() {
        MongoDatabase db = ConnectionMongoCluster.getDatabase(databaseName);
        MongoCollection<Document> collection = db.getCollection(collectionName);
        // count
        System.out.println("documents of the collection "+collectionName+" from the database "+databaseName+": "+
                collection.countDocuments());
        // delete
        collection.drop();
        System.out.println("collection "+collectionName+" deleted...");
    }

    /**
     * Give an array of books from a json file
     * @param file json file path
     * @return Book[]
     */

    public static Book[] getBooksFromFile(String file) {
        // gson
        Gson gson = new Gson();
        // file
        File fileF = new File(file);
        // readers
        String books = "";
        try {
            Scanner reader  = new Scanner(fileF);
            while(reader.hasNextLine()){
                books += reader.nextLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        // save in object Product
        return gson.fromJson(books, Book[].class);
    }

    /**
     * insert an array of book to the mongo DDBB
     * @param books Book[]
     */

    public static void insertObjects(Book[] books) {
        MongoDatabase db = ConnectionMongoCluster.getDatabase("itb").withCodecRegistry(bookRegistry);
        MongoCollection<Book> collection = db.getCollection(collectionName, Book.class);
        InsertManyResult result = collection.insertMany(Arrays.asList(books));
        System.out.println("Books succesfully inserted!\nshowing id for each inserted object: ");
        result.getInsertedIds().forEach((index, value) -> System.out.println(index + " Object id: "+value.asObjectId().getValue()));
    }

    public static void insertObject(Book book) {
        MongoDatabase db = ConnectionMongoCluster.getDatabase("itb").withCodecRegistry(bookRegistry);
        MongoCollection<Book> collection = db.getCollection(collectionName, Book.class);
        try {
            InsertOneResult result = collection.insertOne(book);
            System.out.println("Book succesfully inserted!\nshowing id for the object: ");
            System.out.println(" Object id: "+ Objects.requireNonNull(result.getInsertedId()).asObjectId().getValue());
        } catch (MongoException e) {
            System.out.println("\n\u001B[31m"+e.getMessage()+"\u001B[30m");
        }
    }

    public static List<String> getAllLongDescriptions() {
        MongoDatabase db = ConnectionMongoCluster.getDatabase("itb").withCodecRegistry(bookRegistry);
        MongoCollection<Book> collection = db.getCollection(collectionName, Book.class);
        List<String> descriptions = new ArrayList<>();
        //
//        Bson projections  = Projections.fields(Projections.include("email","phone","phone_aux"),
//                Projections.excludeId());
        FindIterable<Book> query = collection.find();

        query.forEach(book -> descriptions.add(book.getLongDescription()));
        return descriptions;
    }

    public static List<Book> getAllBooks() {
        MongoDatabase db = ConnectionMongoCluster.getDatabase("itb").withCodecRegistry(bookRegistry);
        MongoCollection<Book> collection = db.getCollection(collectionName, Book.class);
        List<Book> books = new ArrayList<>();
        // query
        FindIterable<Book> result = collection.find();
        result.forEach(books::add);
        return books;
    }

    public static List<Book> getBooksByAuthor(String author) {
        List<Book> books = getAllBooks();
        // query
        List<Book> arrayBooks = books.stream().filter(book -> book.getAuthors().stream().anyMatch(auth -> auth.equals(author))).collect(Collectors.toList());
        return arrayBooks;
    }

    public static List<Book> getBooksByAuthors(String[] authors) {
        MongoDatabase db = ConnectionMongoCluster.getDatabase("itb").withCodecRegistry(bookRegistry);
        MongoCollection<Book> collection = db.getCollection(collectionName, Book.class);
        List<Book> books = new ArrayList<>();
        // query
        FindIterable<Book> result = collection.find(all("authors",authors));
        result.forEach(books::add);
        return books;
    }

    public static List<Book> getBooksByPagesAndCategory(double max, double min, String category) {
        MongoDatabase db = ConnectionMongoCluster.getDatabase("itb").withCodecRegistry(bookRegistry);
        MongoCollection<Book> collection = db.getCollection(collectionName, Book.class);
        List<Book> books = new ArrayList<>();
        // query
        FindIterable<Book> result = collection.find(and(gte("pageCount",min),lte("pageCount",max)));
        result.forEach(books::add);
        return books;
    }

    public static List<Book> getBooksByCategoryAndDiscardedAuthor(String category, String author) {
        MongoDatabase db = ConnectionMongoCluster.getDatabase("itb").withCodecRegistry(bookRegistry);
        MongoCollection<Book> collection = db.getCollection(collectionName, Book.class);
        List<Book> books = new ArrayList<>();
        // query
        FindIterable<Book> result = collection.find(and(
                        all("categories",category), not(all("author",author))))
                .sort(Sorts.ascending("title"));
        result.forEach(books::add);
        return books;
    }

    public static List<Book> getBooksByCategories(String[] categories, int limit) {
        MongoDatabase db = ConnectionMongoCluster.getDatabase("itb").withCodecRegistry(bookRegistry);
        MongoCollection<Book> collection = db.getCollection(collectionName, Book.class);
        List<Book> books = new ArrayList<>();
        // query
        FindIterable<Book> result = collection.find(in("categories",categories))
                .sort(Sorts.ascending("title"))
                .limit(limit);
        result.forEach(books::add);
        return books;
    }
    public static Book getBooksByTitle(String title) {
        MongoDatabase db = ConnectionMongoCluster.getDatabase("itb").withCodecRegistry(bookRegistry);
        MongoCollection<Book> collection = db.getCollection(collectionName, Book.class);
        // query
        Book result = collection.find(in("title",title)).first();
        return result;
    }
    public static List<String> getAllCategories() {
        MongoDatabase db = ConnectionMongoCluster.getDatabase("itb").withCodecRegistry(bookRegistry);
        MongoCollection<Book> collection = db.getCollection(collectionName, Book.class);
        // query
        List<String> categories = new ArrayList<>();
        collection.distinct("categories",String.class).forEach(categories::add);
        return categories;
    }

    public static List<String> getAllAuthors() {
        MongoDatabase db = ConnectionMongoCluster.getDatabase("itb").withCodecRegistry(bookRegistry);
        MongoCollection<Book> collection = db.getCollection(collectionName, Book.class);
        // query
        List<String> authors = new ArrayList<>();
        collection.distinct("authors",String.class).forEach(authors::add);
        return authors;
    }

    public static long setAssessment(int value) {
        MongoDatabase db = ConnectionMongoCluster.getDatabase("itb").withCodecRegistry(bookRegistry);
        MongoCollection<Book> collection = db.getCollection(collectionName, Book.class);
        UpdateResult result = collection.updateMany(and(in("categories","Internet")
                ,eq("status","PUBLISH")),set("Assessment",value));
        System.out.println("add Assessment field to all publish books of category Internet ");
        return result.getModifiedCount();
    }

    public static long addAuthor(String author, String title) {
        MongoDatabase db = ConnectionMongoCluster.getDatabase("itb").withCodecRegistry(bookRegistry);
        MongoCollection<Book> collection = db.getCollection(collectionName, Book.class);
        Bson query =eq("title",title);
        Bson updates =push("authors",author);
        UpdateResult result = collection.updateMany(query,updates);
        System.out.println("Add "+author+" author in book with title "+title);
        return result.getModifiedCount();
    }

    public static void createUniqueKeyIndex(String key) {
        MongoDatabase db = ConnectionMongoCluster.getDatabase("itb").withCodecRegistry(bookRegistry);
        MongoCollection<Book> collection = db.getCollection(collectionName, Book.class);
        // indexing
        try{
            IndexOptions indexOptions = new IndexOptions().unique(true);
            String resultCreateIndex =  collection.createIndex(Indexes.ascending(key),indexOptions);
            System.out.printf("Index created: %s%n", resultCreateIndex);
        } catch (DuplicateKeyException e) {
            System.out.printf("duplicate field values encountered, couldn't create index: \t%s\n", e);
        }
    }

    public static void showAllIndex() {
        MongoDatabase db = ConnectionMongoCluster.getDatabase("itb").withCodecRegistry(bookRegistry);
        MongoCollection<Book> collection = db.getCollection(collectionName, Book.class);
        // index
        collection.listIndexes().forEach(System.out::println);
    }

    public static void deleteAllByAuthor(String author) {
        MongoDatabase db = ConnectionMongoCluster.getDatabase("itb").withCodecRegistry(bookRegistry);
        MongoCollection<Book> collection = db.getCollection(collectionName, Book.class);
        // delete
        try {
            DeleteResult result  = collection.deleteMany(in("authors",author));
            System.out.println(author+" Books deleted successfully\n"+
                    "#Deleted-books: "+result.getDeletedCount());
        }catch (MongoException e){
            System.out.println(e.getMessage());
        }
    }

    public static void deleteByTitle(String title) {
        MongoDatabase db = ConnectionMongoCluster.getDatabase("itb").withCodecRegistry(bookRegistry);
        MongoCollection<Book> collection = db.getCollection(collectionName, Book.class);
        // delete
        try {
            DeleteResult result  = collection.deleteOne(eq("title",title));
            System.out.println(title+" book deleted successfully\n"+
                    "#Deleted-books: "+result.getDeletedCount());
        }catch (MongoException e){
            System.out.println(e.getMessage());
        }
    }

    public static void deleteField(String fieldName) {
        MongoDatabase db = ConnectionMongoCluster.getDatabase("itb").withCodecRegistry(bookRegistry);
        MongoCollection<Book> collection = db.getCollection(collectionName, Book.class);
        // delete
        try {
            UpdateResult result = collection.updateMany(ne("_id",-1),unset(fieldName));
            System.out.println(fieldName+" remove from all documents\n#Documents-updated: "+result.getModifiedCount());
        }catch (MongoException e){
            System.out.println(e.getMessage());
        }
    }

    public static void countBooksByAuthor() {
        MongoDatabase db = ConnectionMongoCluster.getDatabase("itb");
        MongoCollection<Document> collection = db.getCollection(collectionName);
        AggregateIterable<Document> queryGrades;
        var settings = JsonWriterSettings.builder()
                .indent(true)
                .outputMode(JsonMode.SHELL)
                .build();
        List<Bson> pipeline = List.of(Aggregates.unwind("$authors"),Aggregates.group("$authors",Accumulators.sum("books",1)));
        List<Document> result = new ArrayList<>();
        queryGrades = collection.aggregate(pipeline);
        queryGrades.forEach(document -> System.out.println(document.toJson()));
    }
}
