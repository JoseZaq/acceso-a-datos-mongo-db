package EA3;

import EA3.CRUD.BooksCRUD;
import EA3.pojo.Book;
import utils.Menu;

import java.util.*;

public class Main {
    private static Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
    public static void main(String[] args) {
        String[] items = {"Ex1-Insert books","Ex2-Query books",
                "Ex3-Update books","Ex4-Indexes","Ex5-Deletes","Ex6-Agregations"};
        Menu menu = new Menu(items, scanner) {
            @Override
            public void goToMethod(int option) {
                switch (option) {
                    case 1:
                        ex1InsertBooks();
                        break;
                    case 2:
                        ex2Cerques();
                        break;
                    case 3:
                        ex3Updates();
                        break;
                    case 4:
                        ex4UniqueKey();
                        break;
                    case 5:
                        ex5Deletes();
                        break;
                    case 6:
                        ex6Agregates();
                        break;
                }
            }
        };
        // start
        menu.start();
    }

    private static void ex6Agregates() {
        BooksCRUD.countBooksByAuthor();
    }

    private static void ex5Deletes() {
        String[] items = {"Ex1- kill Kyle Balley...books","Ex2- kill CoffeHouse book","Ex3- kill status field"};
        Menu menu = new Menu(items, scanner) {
            @Override
            public void goToMethod(int option) {
                switch (option) {
                    case 1:
                        deleteKyleBalleyBooks();
                        break;
                    case 2:
                        deleteCoffeHouseBooks();
                        break;
                    case 3:
                        deleteStatusField();
                        break;
                }
            }
        };
        // start
        menu.start();
    }

    private static void deleteStatusField() {
        BooksCRUD.deleteField("status");
    }

    private static void deleteCoffeHouseBooks() {
        BooksCRUD.deleteByTitle("Coffeehouse");
    }

    private static void deleteKyleBalleyBooks() {
        BooksCRUD.deleteAllByAuthor("Kyle Baley");
    }

    private static void ex4UniqueKey() {
        String[] items = {"Make unique key to isbn field ","show all index","Simulate an error when insert a repeated book"};
        Menu menu = new Menu(items, scanner) {
            @Override
            public void goToMethod(int option) {
                switch (option) {
                    case 1:
                        makeIsbnUnique();
                        break;
                    case 2:
                        showAllIndex();
                        break;
                    case 3:
                        simulateError();
                        break;
                }
            }
        };
        // start
        menu.start();
    }

    private static void simulateError() {
        Book book = new Book();
        book.setIsbn("1933988673");
        book.setAuthors(new ArrayList<String>(List.of("Jose Zaquinaula")));
        BooksCRUD.insertObject(book);
    }

    private static void showAllIndex() {
        BooksCRUD.showAllIndex();
    }

    private static void makeIsbnUnique() {
        BooksCRUD.createUniqueKeyIndex("isbn");
    }

    private static void ex3Updates() {
        String[] items = {"Add Assessment field with value of 5000 to all books with category \"Internet\" and status \"PUBLISH\".",
                "Add author \"Roger Whatch\", to the book \"Unlocking Android\""};
        Menu menu = new Menu(items, scanner) {
            @Override
            public void goToMethod(int option) {
                switch (option) {
                    case 1:
                        addAssessmentField();
                        break;
                    case 2:
                        addAuthor();
                        break;
                }
            }
        };
        // start
        menu.start();
    }

    private static void addAuthor() {
        String author = "Roger Whatch";
        String title = "Unlocking Android";
        long result = BooksCRUD.addAuthor(author,title);
        System.out.println("# of books updated: "+result);
    }

    private static void addAssessmentField() {
        long result = BooksCRUD.setAssessment(5000);
        System.out.println("No of books updated: "+result);
    }

    private static void ex2Cerques() {
        String[] items = {"Show books description","Show title and authors from Danno Ferron books",
                "Show title, authors and pages 300 i 350 pages and have \"Java\" category.",
                "Show title and authors of\"Charlie Collins\" i \"Robi Sen\" books.",
                "Show books of Java category but not of Vikram Goyal author, order by title",
                "Show the first 15 books of categories \"Mobile\" or \"Java\". Order ascendentment by title ",
                "Show all diferent categories of books",
                "Show all diffetent authors of books"};
        Menu menu = new Menu(items, scanner) {
            @Override
            public void goToMethod(int option) {
                String author = "";
                String category = "";
                switch (option) {
                    case 1:
                        printDescriptions();
                        break;
                    case 2:
                        author = "Danno Ferrin";
                        printTitleAndAuthors(author);
                        break;
                    case 3:
                        double max = 350;
                        double min = 300;
                        category = "Java";
                        printByPagesAndCategory(max, min, category);
                        break;
                    case 4:
                        String[] authors = {"Charlie Collins", "Robi Sen"};
                        printByAuthors(authors);
                        break;
                    case 5:
                        author = "Vikram Goyal";
                        category = "Java";
                        printByCategoryAndDiscardedAuthor(category,author);
                        break;
                    case 6:
                        String[] categories = {"Mobile","Java"};
                        int limit = 15;
                        printByCategories(categories, limit);
                        break;
                    case 7:
                        printAllCategories();
                        break;
                    case 8:
                        printAllAuthors();
                        break;
                }
            }
        };
        // start
        menu.start();
    }

    private static void printAllAuthors() {
        List<String> categories = BooksCRUD.getAllAuthors();
        System.out.println("\n=================\nALL AUTHORS OF BOOKS\n=================");
        categories.forEach(author -> System.out.println("- "+author));
    }

    private static void printAllCategories() {
        List<String> categories = BooksCRUD.getAllCategories();
        System.out.println("\n=================\nALL CATEGORIES OF BOOKS\n=================");
        categories.forEach(category -> System.out.println("- "+category));
    }

    private static void printByCategories(String[] categories, int limit) {
        List<Book> books = BooksCRUD.getBooksByCategories(categories,limit);
        books.forEach(System.out::println);
    }

    private static void printByCategoryAndDiscardedAuthor(String category, String discardedAuthor) {
        List<Book> books = BooksCRUD.getBooksByCategoryAndDiscardedAuthor(category, discardedAuthor);
        books.forEach(System.out::println);
    }

    private static void printByAuthors(String[] authors) {
        List<Book> books = BooksCRUD.getBooksByAuthors(authors);
        System.out.println("\n===============");
        for (String author : authors) {
            System.out.print(author.toUpperCase(Locale.ROOT)+" ");
        }
        System.out.print(" BOOKS" );
        for (Book book : books) {
            System.out.println("\n===============");
            System.out.println("Title: "+book.getTitle());
            System.out.println("Authors: ");
            book.getAuthors().forEach(aut -> System.out.println("\t- "+aut));
        }
    }

    private static void printByPagesAndCategory(double max, double min, String category) {
        List<Book> books = BooksCRUD.getBooksByPagesAndCategory(max,min,category);
        System.out.print("BOOKS OF "+ category+ " CATEGORY BETWEEN "+min+" AND "+max +" PAGES");
        for (Book book : books) {
            System.out.println("\n===============");
            System.out.println("Title: "+book.getTitle());
            System.out.println("Authors: ");
            book.getAuthors().forEach(aut -> System.out.println("\t- "+aut));
            System.out.println("PageCount: "+book.getPageCount());
        }
    }

    private static void printTitleAndAuthors(String author) {
        List<Book> books = BooksCRUD.getBooksByAuthor(author);
        for (Book book : books) {
            System.out.println("\n"+author + " books" +
                    "\n===============");
            System.out.println("Title: "+book.getTitle());
            System.out.println("Authors: ");
            book.getAuthors().forEach(aut -> System.out.println("\t- "+aut));
        }
    }

    private static void printDescriptions() {
        List<String> descriptions = BooksCRUD.getAllLongDescriptions();
        descriptions.forEach(desc -> System.out.println("===============\n"+desc));
    }

    private static void ex1InsertBooks() {
        // delete books collection
        BooksCRUD.dropBooks();
        // get and insert books
        Book[] books = BooksCRUD.getBooksFromFile("resources/jsons/books.json");
        BooksCRUD.insertObjects(books);
    }

}
